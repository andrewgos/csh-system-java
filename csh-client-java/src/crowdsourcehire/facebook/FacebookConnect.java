package crowdsourcehire.facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class FacebookConnect {
	public static final String FB_APP_ID = "1658131581085014";
	public static final String FB_APP_SECRET = "52e44d53815d9ed4b532d31ee99e3546";
	public static final String REDIRECT_URI = "http://localhost:8080/csh-system-java/candidate";
	
	public static String getAuthUrl(){
		String authUrl = "";
		try {
			authUrl = "http://www.facebook.com/dialog/oauth?" + "client_id="
					+ FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(REDIRECT_URI, "UTF-8")
					+ "&scope=email";
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
			
		return authUrl;
	}
	
	
	
	public static String getGraphUrl(String code){
		String graphUrl = "";
		try {
			graphUrl = "https://graph.facebook.com/oauth/access_token?"
					+ "client_id=" + FB_APP_ID + "&redirect_uri="
					+ URLEncoder.encode(REDIRECT_URI, "UTF-8")
					+ "&client_secret=" + FB_APP_SECRET + "&code=" + code;	

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return graphUrl;
	}
	
	
	
	public static String getAccessToken(String graphUrl) {
		URL graphURL;
		try {
			graphURL = new URL(graphUrl);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new RuntimeException("Invalid code received " + e);
		}
		URLConnection fbConnection;
		StringBuffer b = null;
		try {
			fbConnection = graphURL.openConnection();
			BufferedReader in;
			in = new BufferedReader(new InputStreamReader(fbConnection.getInputStream()));
			String inputLine;
			b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to connect with Facebook " + e);
		}
		
		String accessToken = b.toString();
		if (accessToken.startsWith("{")) {
			throw new RuntimeException("ERROR: Access Token Invalid: "
					+ accessToken);
		}
	
		return accessToken;
	}
	
	public static String getGraph(String accessToken) {
		String graph = null;
		try {
			String g = "https://graph.facebook.com/me?" + accessToken;
			URL u = new URL(g);
			URLConnection c = u.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			String inputLine;
			StringBuffer b = new StringBuffer();
			while ((inputLine = in.readLine()) != null)
				b.append(inputLine + "\n");
			in.close();
			graph = b.toString();
			//System.out.println(graph);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("ERROR in getting FB graph data. " + e);
		}
		return graph;
	}
}
