package crowdsourcehire.command;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.*;
import java.util.Date;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterCommand implements Command {

	@Override
	public boolean execute(HttpServletRequest request, HttpServletResponse response) {
		
		// CANCELLED, CHANGE THIS WITH DYLAN's MIDDLE LAYER
		/*try {
			Connection connection = DatabaseUtil.connectToDB();
			Statement getUserStatement = connection.createStatement();
			Statement getUserTypeStatement = connection.createStatement();
			Statement getGroupIDStatement = connection.createStatement();
			Statement getBranchIDStatement = connection.createStatement();
			Statement insertStatement = connection.createStatement();
			
			
			// CHECK IF USERNAME OR EMAIL ALREADY EXISTS
			String username = request.getParameter("username");
			String email = request.getParameter("email");
			String getUserQuery = "SELECT UserID, UserName, email from users where email = '"+email+"' or UserName = '"+username+"'";
	        ResultSet resultUser = getUserStatement.executeQuery(getUserQuery);
	        while(resultUser.next()){
	        	if(resultUser.getString("Username").equals(username)){
	        		response.getWriter().print("Username already exists.");
	        	}
	        	else if(resultUser.getString("email").equals(email)){
	        		response.getWriter().print("Email already exists.");
	        	}
	 		    return false;
	        }
	        
	        
	        
				
			UUID guid = UUID.randomUUID();
		    //user_self_registering($arr_insert);
		    int apr=0;
		    
		    HashMap<String, String> map = new HashMap<String, String>();
		    map.put("UserName" , request.getParameter("username").trim());
		    map.put("Password" , md5(request.getParameter("password").trim()));
		    map.put("Name" , request.getParameter("firstname").trim());
		    map.put("company" , request.getParameter("lastname").trim());
		    map.put("added_date" , new Timestamp(new Date().getTime()).toString());
		    String userType = null;
	        String getUserTypeQuery = "SELECT id FROM roles WHERE system_row=2 LIMIT 0,1";
	        ResultSet resultUserType = getUserTypeStatement.executeQuery(getUserTypeQuery);
	        while(resultUserType.next()){
	        	 userType = resultUserType.getString("id");
	        }
		    map.put("user_type" , userType);
		    map.put("email" , request.getParameter("email").trim());
		    map.put("user_type" , request.getParameter("userType").trim());
		    map.put("address" , "");
		    map.put("phone" , "");
		    map.put("country_id" , "");
		    map.put("approved" , String.valueOf(apr));
		    map.put("disabled" , "0");
		    map.put("random_str" , guid.toString());
		    map.put("self_registered" , "0");
		    
		    String group_id = null;
	        String getGroupIDQuery = "SELECT id FROM user_groups WHERE is_default=1 LIMIT 0,1";
	        ResultSet resultGroupID = getGroupIDStatement.executeQuery(getGroupIDQuery);
	        while(resultGroupID.next()){
	        	group_id = resultGroupID.getString("id");
	        }
		    map.put("group_id" , group_id);
		    
		    String branch_id = null;
	        String getBranchIDQuery = "SELECT id FROM branches WHERE system_row=1 LIMIT 0,1";
	        ResultSet resultBranchID = getBranchIDStatement.executeQuery(getBranchIDQuery);
	        while(resultBranchID.next()){
	        	branch_id = resultBranchID.getString("id");
	        }
		    map.put("branch_id" , branch_id);
		    
		    
		    
		    String insertQuery = DatabaseUtil.getInsertQuery("users", map);
		    insertStatement.executeUpdate(insertQuery);
		   
		    //set url for email confirm
		    String url = request.getRequestURL().toString() + "register.php?step=3&g=" + guid;
			//$url = WEB_SITE_URL."register.php?step=3&g=".$guid;	

		    
		    
			try {
			    MailSender sender = null;
				sender = MailSender.getMailSender();
				String fromAddress = "support@crowdsourcehire.com";
				String toAddress = email;
				String subject = "Welcome to CrowdSourceHire - User Registration";
				StringBuffer mailBody = new StringBuffer();
				mailBody.append("Hi "+request.getParameter("firstname")+","+"<br>"
						+ "Thank you for signing up with CrowdSourceHire. Here are the details of your registration : "+"<br>"
						+ "Name : " + request.getParameter("firstname") +"<br>"
						+ "Company Name : " + request.getParameter("lastname") +"<br>"
						+ "Login : " + request.getParameter("username") +"<br>"
						+ "To activate your account with CrowdSourceHire , please click on the link below." +"<br>"
						+ url +"<br>"
						+ "Thank you \n The CrowdSourceHire Team");
	
				sender.sendMessage(fromAddress, toAddress, subject, mailBody);
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (ServiceLocatorException | MailSenderException e) {
				e.printStackTrace();
			}
			

			connection.close();
			getUserStatement.close();
			getUserTypeStatement.close();
			getGroupIDStatement.close();
			getBranchIDStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
*/
		
		return false;
	}
	
	
	public String md5(String input) {
		String hashed = null;

		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
			hashed = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashed;
	}
	
	
	

}
