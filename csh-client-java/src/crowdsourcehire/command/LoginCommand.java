package crowdsourcehire.command;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.signature.AuthorizationHeaderSigningStrategy;
import oauth.signpost.signature.HmacSha1MessageSigner;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.code.linkedinapi.client.constant.ApplicationConstants;
import com.google.code.linkedinapi.client.constant.LinkedInApiUrls;
import com.google.code.linkedinapi.client.enumeration.HttpMethod;





import crowdsourcehire.bean.Candidate;
import crowdsourcehire.facebook.FacebookConnect;



public class LoginCommand implements Command {
	
	public static final String LINKEDIN_KEY = "752qva0klvc1v8";    //add your LinkedIn key
	public static final String LINKEDIN_SECRET = "SIxFGT4jaQeJa6X8"; //add your LinkedIn Secret
	
    private static final String OAUTH_VERSION_1_0_a = "1.0a";
    
	@Override
	public boolean execute(HttpServletRequest request, HttpServletResponse response) {
		
		if(request.getParameter("linkedinLogin") != null){			        
	        // SET CONSUMER
	        OAuthConsumer consumer = new DefaultOAuthConsumer(LINKEDIN_KEY, LINKEDIN_SECRET);
            consumer.setMessageSigner(new HmacSha1MessageSigner());
            consumer.setSigningStrategy(new AuthorizationHeaderSigningStrategy());
            
            // SET PROVIDER
            OAuthProvider provider = new DefaultOAuthProvider(LinkedInApiUrls.LINKED_IN_OAUTH_REQUEST_TOKEN_URL,
                    LinkedInApiUrls.LINKED_IN_OAUTH_ACCESS_TOKEN_URL, LinkedInApiUrls.LINKED_IN_OAUTH_AUTHORIZE_URL);
		    provider.setOAuth10a(OAUTH_VERSION_1_0_a.equals(ApplicationConstants.OAUTH_VERSION));

		    // GET URL
		    String callBackUrl = "http://localhost:8080/csh-system-java/candidate";
		    try {
				String authorizationUrl = provider.retrieveRequestToken(consumer, callBackUrl);
				response.sendRedirect(authorizationUrl);
				
		        request.getSession().setAttribute("consumer", consumer);
		        request.getSession().setAttribute("provider", provider);

			} catch (OAuthMessageSignerException e) {
				e.printStackTrace();
			} catch (OAuthNotAuthorizedException e) {
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

	        
		}
		
		if(request.getParameter("oauth_token") != null && request.getParameter("oauth_verifier") != null){			
			String oauthVerifier = request.getParameter("oauth_verifier");
			
			// GET CONSUMER AND PROVIDER
	        OAuthConsumer consumer = (OAuthConsumer) request.getSession().getAttribute("consumer");
            OAuthProvider provider = (OAuthProvider) request.getSession().getAttribute("provider");
	            
            consumer.setTokenWithSecret(consumer.getToken(), consumer.getTokenSecret());
	        try {
				provider.retrieveAccessToken(consumer, oauthVerifier);
				
				ObjectMapper mapper = new ObjectMapper();
           		Candidate candidate = new Candidate();
           		
				
				String responseGetEmail = callService(consumer, HttpMethod.GET.fieldName(),
						"https://api.linkedin.com/v1/people/~:(id,email-address)?format=json");
				//"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,email-address,picture-url,positions)?format=json");
				//"https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,industry,positions,educations)?format=json");
				//"https://api.linkedin.com/v1/people/~:(id,first-name,skills,educations,languages,twitter-accounts)?format=json");
				System.out.println(responseGetEmail);
				JsonNode linkedinNode = mapper.readTree(responseGetEmail);
				
				String email = linkedinNode.get("emailAddress").getTextValue();

				String emailTEMP = "andrewgosali@gmail.com";
				String responseCheckEmail = callService(HttpMethod.GET.fieldName(), "http://localhost:8080/JAXRS-HelloWorld/rest/REST/checkUser?email="+emailTEMP);

				
				// IF EMAIL IS NEW
				if(!isJSONValid(responseCheckEmail) || responseCheckEmail.equals("[]")){
					// UNDER CONSTRUCTION
					// TODO calling middle layer to create new account
					
					// SET THE SESSION FOR THIS NEW ACCOUNT
					//request.getSession().setAttribute("userID", userID);
					response.sendRedirect("candidate/candidate-step1.php");
				}
				// IF EMAIL IS EXISTS
				else{
					JsonNode restNode = mapper.readTree(responseCheckEmail);
					String userID = restNode.get("UserID").getTextValue();
			        request.getSession().setAttribute("userID", userID);
					response.sendRedirect("candidate/candidate-profile.php?id="+userID);
				}

				    
				    
				
				
           		/*candidate.setId(rootNode.get("id").getTextValue());
				candidate.setFirstName(rootNode.get("firstName").getTextValue());
				candidate.setLastName(rootNode.get("lastName").getTextValue());
				candidate.setHeadline(rootNode.get("headline").getTextValue());
				candidate.setEmail(rootNode.get("emailAddress").getTextValue());
				candidate.setPictureUrl(rootNode.get("pictureUrl").getTextValue());

				request.getSession().setAttribute("candidate", candidate);
				response.sendRedirect("candidate/candidate_profile.jsp");*/
				
				
           
    			
			} catch (OAuthMessageSignerException e) {
				e.printStackTrace();
			} catch (OAuthNotAuthorizedException e) {
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (ProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		
		
		if(request.getParameter("facebookLogin") != null){		
			try {
				String authUrl = FacebookConnect.getAuthUrl();
				response.sendRedirect(authUrl);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		if(request.getParameter("code") != null){
			String code = request.getParameter("code");
			String graphUrl = FacebookConnect.getGraphUrl(code);
			String accessToken = FacebookConnect.getAccessToken(graphUrl);
			String graph = FacebookConnect.getGraph(accessToken);
			
			try {
				JSONObject json = new JSONObject(graph);
				Candidate candidate = new Candidate();
				candidate.setFirstName(json.getString("first_name"));
				candidate.setLastName(json.getString("last_name"));
				candidate.setPictureUrl("http://graph.facebook.com/"+json.getString("id")+"/picture?type=large");
				candidate.setEmail(json.getString("email"));
				
				/*System.out.println(json.getString("id"));
				System.out.println(json.getString("first_name"));
				System.out.println(json.getString("email"));
				System.out.println(json.getString("gender"));*/
				
				request.getSession().setAttribute("candidate", candidate);
				response.sendRedirect("candidate/candidate_profile.jsp");

			} catch (JSONException e) {
				e.printStackTrace();
				throw new RuntimeException("ERROR in parsing FB graph data. " + e);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
		}

		
		
		return false;
	}
	
	
	public String callService(OAuthConsumer consumer, String method, String urlPath ){
		String responseBody = "";
		try {
			String accessToken = consumer.getToken();
	        String accessTokenSecret = consumer.getTokenSecret();
	        
			// CALL SERVICE
	        URL url = new URL(urlPath);
			
	        HttpURLConnection requestLinkedin = (HttpURLConnection) url.openConnection();
	        requestLinkedin.setRequestMethod(method);
	        requestLinkedin.setDoOutput(true);
	        consumer.setMessageSigner(new HmacSha1MessageSigner());
	        consumer.setSigningStrategy(new AuthorizationHeaderSigningStrategy());
	        consumer.setTokenWithSecret(accessToken, accessTokenSecret);
	        consumer.sign(requestLinkedin);
	        requestLinkedin.connect();
	        
	        // GET RESPONSE BODY
	        responseBody = IOUtils.toString(requestLinkedin.getInputStream(), requestLinkedin.getContentEncoding());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OAuthMessageSignerException e) {
			e.printStackTrace();
		} catch (OAuthExpectationFailedException e) {
			e.printStackTrace();
		} catch (OAuthCommunicationException e) {
			e.printStackTrace();
		}
		return responseBody;
	}
	
	public String callService(String method, String urlPath ){
		String responseBody = "";
		try {	        
			// CALL SERVICE
	        URL url = new URL(urlPath);
			
	        HttpURLConnection request = (HttpURLConnection) url.openConnection();
	        request.setRequestMethod(method);
	        request.setDoOutput(true);
	        
	        request.connect();
	        
	        // GET RESPONSE BODY
	        responseBody = IOUtils.toString(request.getInputStream(), request.getContentEncoding());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseBody;
	}
	
	public boolean isJSONValid(String test) {
	    try {
	        new JSONObject(test);
	    } catch (JSONException ex) {
	        return false;
	    }
	    return true;
	}
}

