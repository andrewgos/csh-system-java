package crowdsourcehire.control;

import java.io.*;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import crowdsourcehire.command.Command;
import crowdsourcehire.command.LoginCommand;
import crowdsourcehire.command.RegisterCommand;
import crowdsourcehire.command.ViewCommand;

/**
 * Servlet implementation class AdminControlServlet
 */
@WebServlet("/expert")
public class ExpertControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private HashMap<String, Command> commandMap = new HashMap<String,Command>();

    /**
     * Default constructor. 
     */
    public ExpertControlServlet() {
        commandMap.put("login", new LoginCommand());
        commandMap.put("register", new RegisterCommand());
        commandMap.put("view", new ViewCommand());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nextPage = "expert_dashboard.jsp";
		
		if(request.getParameter("viewCandidateList") != null){
			nextPage = "expert_dashboard.jsp";
		}
		
		
		// jsp page name appear
		response.sendRedirect(nextPage);
		
		// the query param appear
		//RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		//rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "expert_dashboard.jsp";
		boolean isSuccess = false;


		if(request.getParameter("doLogin") != null){
			isSuccess = commandMap.get("login").execute(request, response);
			if(isSuccess) nextPage = "expert_dashboard.jsp"; 
			else nextPage = "expert_authenticate.jsp";
		}
		if(request.getParameter("doRegister") != null){
			isSuccess = commandMap.get("register").execute(request, response);
			if(isSuccess) nextPage = "expert_dashboard.jsp"; 
			else nextPage = "expert_authenticate.jsp";
		}
		
		response.sendRedirect(nextPage);

	}
	

}
