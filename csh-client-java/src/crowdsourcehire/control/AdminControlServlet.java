package crowdsourcehire.control;

import java.io.*;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import crowdsourcehire.command.Command;
import crowdsourcehire.command.LoginCommand;
import crowdsourcehire.command.ViewCommand;

/**
 * Servlet implementation class AdminControlServlet
 */
@WebServlet("/admin")
public class AdminControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private HashMap<String, Command> commandMap = new HashMap<String,Command>();

    /**
     * Default constructor. 
     */
    public AdminControlServlet() {
        commandMap.put("login", new LoginCommand());
        commandMap.put("view", new ViewCommand());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nextPage = "admin_dashboard.jsp";
		
		if(request.getParameter("viewCompanyList") != null){
			
			nextPage = "admin_company_list.jsp";
		}
		else if(request.getParameter("viewExpertList") != null){
			nextPage = "admin_expert_list.jsp";
		}
		else if(request.getParameter("viewCandidateList") != null){
			nextPage = "admin_candidate_list.jsp";
		}
		else if(request.getParameter("viewAssessment") != null){
			nextPage = "admin_assessment.jsp";
		}
		else if(request.getParameter("viewPayment") != null){
			nextPage = "admin_payment.jsp";
		}
		
		// jsp page name appear
		response.sendRedirect(nextPage);
		
		// the query param appear
		//RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		//rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "admin_dashboard.jsp";
		boolean isSuccess = false;

		if(request.getParameter("doLogin") != null){
			isSuccess = commandMap.get("login").execute(request, response);
			if(isSuccess) nextPage = "admin_dashboard.jsp"; 
			else nextPage = "admin_authenticate.jsp";
		}
		
		response.sendRedirect(nextPage);

	}
	

}
