package crowdsourcehire.control;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import crowdsourcehire.command.Command;
import crowdsourcehire.command.LoginCommand;
import crowdsourcehire.command.RegisterCommand;
import crowdsourcehire.command.ViewCommand;

/**
 * Servlet implementation class CompanyControlServlet
 */
@WebServlet("/candidate")
public class CandidateControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HashMap<String, Command> commandMap = new HashMap<String,Command>();

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CandidateControlServlet() {
        super();
        commandMap.put("login", new LoginCommand());
        commandMap.put("register", new RegisterCommand());
        commandMap.put("view", new ViewCommand());    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "company_dashboard.jsp";
		boolean isSuccess = false;
		
		if(request.getParameter("linkedinLogin") != null){
			isSuccess = commandMap.get("login").execute(request, response);
		}
		if(request.getParameter("oauth_token") != null && request.getParameter("oauth_verifier") != null){
			isSuccess = commandMap.get("login").execute(request, response);
		}
		
		if(request.getParameter("facebookLogin") != null){
			isSuccess = commandMap.get("login").execute(request, response);
		}
		if(request.getParameter("code") != null){
			isSuccess = commandMap.get("login").execute(request, response);
		}
			
		
		// jsp page name appear
		//response.sendRedirect(nextPage);
		
		// the query param appear
		//RequestDispatcher rd = request.getRequestDispatcher("/"+nextPage);
		//rd.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nextPage = "candidate_dashboard.jsp";
		boolean isSuccess = false;

		
		
		if(request.getParameter("doRegister") != null){
			isSuccess = commandMap.get("register").execute(request, response);
			// THE REDIRECTION IS IN COMPANY_AUTHENTICATE.JSP PAGE
			//if(isSuccess) nextPage = "company_dashboard.jsp";
			//else nextPage = "company_authenticate.jsp";
		}
		
		//response.sendRedirect(nextPage);
		
	}

}
