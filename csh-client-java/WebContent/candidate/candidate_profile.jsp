<jsp:include page="candidate_profile_header.jsp" />

<body class="<?php echo $page_name ?>">
<div class="site-wrapper">
	<div class="header-wrapper parallax">
    	<div class="page-wrapper">
    		<div class="header">
            	<div class="mid-header">
    				<span class="top-menu-btn menu-btn"><img src="images/menu-icon.png" width="50" height="50" alt="" title="" /></span>
                    <div class="mid-text-box"> 
                    	<div class="rounded-box">
                        	<span class="hi-txt">Hi</span>
                            
                        </div>
                        <h1>I'm ${candidate.firstName} ${candidate.lastName}</h1>
                            <h2><?php echo $linked_profile_url['headline'] ?></h2>
                    </div>
                </div>
        	</div><!-- header end here -->
        </div><!-- page-wrapper end here-->
        
	</div><!--site-wrapper end here-->
    
    <div class="navbar-wrapper mobile-menu">
    	<div class="page-wrapper">
        <div class="navbar">
        
        	<div class="logo"><a href="index.html"><img src="images/logo.png" alt="Crowd Source Hire" title="Crowd Source Hire" /></a></div>
        	<div class="nav">
            	<!--<span class="menu-btn"><img src="images/menu-btn.png" alt="" title="" /></span>-->
            	<ul class="menu">
            		<li><a href="#1">Assessment Summary</a></li>
                    <li><a href="#2">Skill</a></li>
                    <li><a href="#3">Job History</a></li>
                    <li><a href="#4">Education</a></li>
                    <li class="active"><a href="http://www.crowdsourcehire.com/expert.html">Join As Expert</a></li>
                </ul>
                
            </div>
            </div>
        </div><!--page-wrapper end here-->
    </div><!--navbar-wrapper end here-->
    
    
    
    
    
    <div class="profile-wrapper">
    	<div class="page-wrapper">
        	<div class="profile-section">
            	<div class="proile-lft-section">
                	<span class="profile-dp"><img src="${candidate.pictureUrl}" alt="" /></span>
                    <?php 
					if(empty($hosted_details->upload_file_name)){
						
					}
					else{
					?>
					
                    <a target="_blank" class="download-btn" href="<?php echo $site_path2 ?>ios/ajaxrequest/uploads/<?php echo $hosted_details->upload_file_name ?>">Download Resume</a>
					
					<?php  } ?> 
                
                </div>
                <div class="proile-rt-section">
                <h2>${ candidate.firstName} ${ candidate.lastName}</h2>
                
                <div class="location-sec">
                	<div class="email-sec">
                    	<span class="profile-icons"><img src="images/msg.png" alt="" /></span>
                        <span class="email-text"><a href="mailto:${candidate.email}">${candidate.email}</a></span>
                    </div>
                    <div class="location-sec">
                    	<span class="profile-icons"><img src="images/location.png" alt="" /></span>
                    	<span class="location-text"><?php echo $linked_profile_url['location'] ?></span>
                    </div>
                </div>
                

                </div>
            </div><!--profile-section end here-->
			<div style="clear:both;"></div>
			<div class="carrer-sec profile-section">
			                <div class="btns-section">
                	<ul class="btn-list">
                    	<li style="display:none;"><a href="javascript:;">
                        	<div class="white-pic-bx"><img src="images/cup-icon.png" alt="" width="15" height="18" /></div>
                            <span class="btn-text-01">Top 1%</span>
                        </a></li>
                        <li><a href="javascript:;">
                            <span class="btn-text-02"> Open to job Opportunities<br />

<!-- <span>$freelance-full time-part-time</span> -->
</span>
                        </a></li>
                        
                 <!--       <li><a href="javascript:;">
                        	<div class="white-pic-bx"><img src="images/dollor-icon.png" alt="" width="25" height="25" /></div>
                            <span class="btn-text-03">Salary<br />

<span>$salary</span></span>
                        </a></li>  -->
                        
                        
                    </ul>
                   
                </div>
                 <h3>Career Objectives</h3>
                            <p class="showpara"><?php  echo limit_words($carrer_text_less,40); ?> ....<a style="" href="javascript:;" class="read_more"><br>
Read More</a></p>
                            <p style="display:none" class="hiddenpara"><?php echo $carrer_text_less ?> <a class="read_more" href="javascript:;" style=""><br>Less</a></p>
			</div>
        </div><!--page-wrapper end here-->
    </div><!--profile-wrapper end here-->
    
    <div class="summry-wrapper">
    	<div class="page-wrapper">
    		<div class="summry-section">
            	<div class="inr-summry-sec"  id="1">
                	<div class="heading-section"><div class="heading-bx-01">
                    	<span class="left-icon"><img src="images/assessment-icon.png" alt="" /></span>
                    	<h2>Assessment Summary</h2>
                             
                                

                    </div>
                    
                    </div>
                    
					
                    <div class="top-date-sec">
                    <div class="data-analyst-sec">
                    	<div class="left-date-sec">
                        	<h3><?php echo $assessment_name ?></h3>
                            <ul class="data-list">
                            	<li><span class="data-icons"><img src="images/date-icon.png" alt="" /></span><?php echo $assessement_date ?></li>
                                <li><span class="data-icons"><img src="images/time-icon.png" alt="" /></span><?php echo $assessement_time ?> </li>
         <li><span class="data-icons"><img src="<?php  echo $actual_link = "http://$_SERVER[HTTP_HOST]"; ?>/challenge/dev_user_profile/images/url-icon.png" alt="" /></span><?php if($hosted_details->code_url){?><a target="_blank" href="<?php if($hosted_details->code_url){echo (strpos($hosted_details->code_url, 'http')) ? 'http://' : 'http://';}echo str_replace('https://', '', str_replace('http://', '', $hosted_details->code_url));?>">Hosted URL for the application</a><?php }else{  echo 'None Submitted'; }?></li>
                                <li><span class="data-icons"><img src="<?php  echo $actual_link = "http://$_SERVER[HTTP_HOST]"; ?>/challenge/dev_user_profile/images/video-icon.png" alt="" /></span><?php if($hosted_details->video_demo_url){?><a target="_blank" href="<?php if($hosted_details->video_demo_url){echo (strpos($hosted_details->video_demo_url, 'http')) ? 'http://' : 'http://';}echo str_replace('https://', '', str_replace('http://', '', $hosted_details->video_demo_url));?>">Hosted video demo url</a><?php }else{  echo 'None Submitted'; }?> </li>
                            </ul>
                        </div>

						<button class="popup-opens" data-toggle="modal" href="#responsive">View Problem</button>
                        <a href="javascript:;" class="view-result-btn view-result" style="display:none;">View  Result</a>
                        <a href="javascript:;" class="view-result-btn hide-result">Hide Result</a>
                        
                    </div>

                	</div>
                    
                    <div class="slidershowonclick">
                    <div id="w">
                    
                      
                      <div class="slider">
                        <ul class="slides">
						<?php 
/* 						echo '<pre>';				
print_r($assessment_answerss); 
echo '</pre>';	   */
						
						foreach($assessment_answerss as $assessment_answers){
							?>
                          <li class="slide">
                          <h3>Assessment Answers  </h3>
						  <div style="clear:both;"></div>
						  <div class="question-outter">
						    <h3 style="color:#458EC6;">Question</h3>
   <div class="video-sec-question">                    
<?php echo $assessment_answers['question_text'] ?>?</h4>
</div>
</div>
<div class="answer-sec">
 <div class="answer-outter">
<h3 style="color:#458EC6;">Answer</h3>
<?php 
if(!empty($assessment_answers['videofilename'])){
 ?>
<div class="video-sec">
<object width="260" height="180" id="p1111_api" name="p1111_api" data="../flowplayer/flowplayer-3.2.16.swf" type="application/x-shockwave-flash"><param name="allowfullscreen" value="true"><param name="allowscriptaccess" value="always"><param name="quality" value="high"><param name="bgcolor" value="#000000"><param name="flashvars" value="config={&quot;clip&quot;:{&quot;autoPlay&quot;:false,&quot;autoBuffering&quot;:true,&quot;url&quot;:&quot;http://video.crowdsourcehire.com/dev/<?php echo $assessment_answers['videofilename'];  ?>&quot;},&quot;playerId&quot;:&quot;p1111&quot;,&quot;playlist&quot;:[{&quot;autoPlay&quot;:false,&quot;autoBuffering&quot;:true,&quot;url&quot;:&quot;http://video.crowdsourcehire.com/dev/<?php echo $assessment_answers['videofilename'];  ?>  &quot;}]}"></object>
</div>
<?php } ?>
<?php 
if(!empty($assessment_answers['user_answer_text'])){
 ?>
<div class="video-sec-text">
<?php 
echo $assessment_answers['user_answer_text'];
?>
</div>
<?php } ?> 
</div>
</div> 
                           </li>     
						<?php } ?>						   
                        </ul>
                      </div><!-- @end .slider -->
                    </div>
					
					</div>
                </div>
                
                <div class="inr-summry-sec"  id="2">
			<!--	<div class="overlay"></div>    -->
                	<div class="heading-section"><div class="heading-bx-02">
                    	<span class="left-icon"><img src="images/assessment-icon.png" alt="" /></span>
                    	<h2>Skills</h2>
                    </div>
                    </div>
                   
                    <div class="skills-section">
                    <div class="left-skills">
                    	<span class="chart"><div id="container-chart" style="min-width: 310px; max-width: 400px; height: 400px; margin: 0 auto"></div></span>
                        <p>PS: Percentile rank refers to the percentage of scores that are equal to or less than a giv en score. Percentile ranks, like percentages, fall on a continuum from 0 to 100. For example, a percentile rank of 35 indicates that 35% of the scores in a distribution of scores fall at or below the score at the 35th percentile.</p>
                    
                    </div>

                    <div class="right-skills">

                    </div>
                    
                    <div class="divider"></div>
                    <!--<h3 class="skill-set-heaing">Skill Set</h3>
                    <div class="linkedin-profile">
                    	<div class="linked-box">
                        	<span class="linked-icon"><a href="javascript:;"><img src="<?php //echo $site_path ?>images/linkedin.png" alt="" /></a></span>
                            Linkedin Profile
                        </div>
                    </div>
                    	<div class="language-set">
                        	<ul class="language-list">
							<?php//  foreach($skill_array as $key => $value){ 
							?>
                            	<li><a href="javascript:;"><?php// echo $value ?></a></li>
							<?php //} ?>
                            </ul>
                        </div>
                    -->
                    </div>
				
                </div>
				
				<div class="skill-set skills-section">
					<h3 class="skill-set-heaing">Skill Set</h3>
                    <div class="linkedin-profile">
                    	<div class="linked-box">
                        	<span class="linked-icon"><a href="<?php if(empty($linked_profile_url['public-profile-url'])){ echo 'javascript:;';} else{echo $linked_profile_url['public-profile-url'];} ?>"><img src="images/linkedin.png" alt="" /></span>
                            Linkedin Profile</a>
                        </div>
                    </div>
                    	<div class="language-set">
                        	<ul class="language-list">
							<?php  foreach($skill_array as $key => $value){ 
							?>
                            	<li><a href="javascript:;"><?php echo $value ?></a></li>
							<?php } ?>
                            </ul>
                        </div>
				</div>
				
                <div class="inr-summry-sec"  id="3">
                	<div class="heading-section"><div class="heading-bx-03">
                    	<span class="left-icon"><img src="images/job-icon.png" alt="" /></span>
                    	<h2>Job History</h2>
                    </div>
                    </div>
                    <span class="divider"></span>
                    <div class="job-histroy">
                    	<div class="left-job-sec">
                        	
							<?php 
							                                foreach($work_expriance as $key => $value){

                                    $group_details=$profile->get_user_profile_by_group_id($value['group_id']);
									?>
                        	<div class="mian-left-h-sec">
                            	<div class="left-job-histry">
                                	<h3><?php echo $group_details['company_name']; ?></h3>
                                    <p><?php echo $group_details['company_designation']; ?></p>
                                </div>
                                <div class="left-job-histry">
                                	<span class="gray-circle"></span>
                                    <span class="current-date"><?php echo $group_details['start_year']; ?>-<?php echo $group_details['end_year']; ?></span>
<span class="current-date"><?php echo $group_details['jobtype']; ?></span>
                                </div>
								                        <div class="right-job-sec">

                        	<p><?php echo $group_details['describtion']; ?></p>
                        </div>
                                
                            
                            </div>
															<?php  } ?>
                        </div>
                        

                    </div>
                    
                </div>
              
                
               <div class="inr-summry-sec"  id="4">
                	<div class="heading-section"><div class="heading-bx-04">
                    	<span class="left-icon"><img src="images/education.png" alt="" /></span>
                    	<h2>Education</h2>
                    </div>
                    
                    </div>
                    <span class="divider"></span>
                    
                    <div class="education-section">
                    	<div class="mid-edu-sec">
			<?php 
                                  foreach($education_details as $key => $value){

                                    $group_details=$profile->get_user_profile_by_group_id($value['group_id']);
                                ?>
                        <div class="years-detail-sec">
                        <div class="blue-wrapper">
                        	<div class="blue-box">
                            	<h3><span><?php echo $group_details['start_year']; ?><br />

-<br />

<?php echo $group_details['end_year']; ?></span></h3>
                            	<span class="down-arrow"></span>
                            </div>
                            </div>
                            <h3><?php echo $group_details['university_name']; ?></h3>
                            <p><?php echo $group_details['describtion']; ?></p>
                           </div>
								  <?php } ?>
                        </div>           
                    </div>
                </div>
    		
        	</div><!-- summry section end here -->
			<div style="display:none;" class="nav_up" id="nav_up"></div>
        </div><!-- page-wrapper end here -->
    </div><!--summry-wrapper end here-->

<div id="responsive" class="modal hide fade" tabindex="-1" data-width="760">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<!--   <div class="assess-problems">
           <h6>The <span>Problem</span></h6>
		   </div>  -->
  </div>
  <div class="modal-body">
           <div class="assess-problems">
           <h6>The <span>Problem</span></h6>
					<ul style="color:#000" class="list-rules list-rules2">
					<?php 
					$i=1;
					foreach($assignments_problems as $assignments_problems){
						echo '<li><span class="lispace1">'.$i.'.)</span><p class="lispace2">'.$assignments_problems['problem'].'</p></li>';
						$i++;
					}
?>                   
                     </ul>
					</div>
  </div>
  <div class="modal-footer">
               <div class="summry-section">
               <div class="top-date-sec">
                    <div class="data-analyst-sec">
                    	<div class="left-date-sec">
                        	<h3><?php echo $assessment_name ?></h3>
                            <ul class="data-list">
                            	<li><span class="data-icons"><img src="images/date-icon.png" alt="" /></span><?php echo $assessement_date ?></li>
                                <li><span class="data-icons"><img src="images/time-icon.png" alt="" /></span><?php echo $assessement_time ?> </li>
         <li><span class="data-icons"><img src="http://dev.crowdsourcehire.com/challenge/dev_user_profile/images/url-icon.png" alt="ih" /></span><?php if($hosted_details->code_url){?><a target="_blank" href="<?php if($hosted_details->code_url){echo (strpos($hosted_details->code_url, 'http')) ? 'http://' : 'http://';}echo str_replace('https://', '', str_replace('http://', '', $hosted_details->code_url));?>">Hosted URL for the application</a><?php }else{  echo 'None Submitted'; }?></li>
                                <li><span class="data-icons"><img src="<?php  echo $actual_link = "http://$_SERVER[HTTP_HOST]"; ?>/challenge/dev_user_profile/images/video-icon.png" alt="" /></span>
								<?php if($hosted_details->video_demo_url){?><a target="_blank" href="<?php if($hosted_details->video_demo_url){echo (strpos($hosted_details->video_demo_url, 'http')) ? 'http://' : 'http://';}echo str_replace('https://', '', str_replace('http://', '', $hosted_details->video_demo_url));?>">Hosted video demo url</a><?php }
								else{  echo 'None Submitted';}?> </li>
                            </ul>
                        </div>
                        
                    </div>

                	</div>
                	</div>
  </div>
</div>


<jsp:include page="candidate_profile_footer.jsp" />