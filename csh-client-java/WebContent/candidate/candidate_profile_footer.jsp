  <div class="footer-wrapper">
    	<div class="page-wrapper">
    		<div class="footer-section">
            	<span class="f-logo"><a href="index.html"><img src="<?php echo $site_path ?>images/footer-logo.png" alt="" title="" /></a></span>
                <div class="mid-footer">
                	<ul class="f-menu">
                    	<li><a href="javascript:;">Who we are</a></li>
                        <li><a href="javascript:;">Contact us</a></li>
                        <li><a href="javascript:;">How it works</a></li>
                        <li><a href="javascript:;">Faq</a></li>
                        <li><a href="javascript:;">Become an Expert</a></li>
                        <li><a href="javascript:;">Legal</a></li>
                    </ul>
                    <p>Email: <a href="mailto:info@crowdsourcehire.com">info@crowdsourcehire.com</a></p>
                </div>
                
                <div class="right-footer">
                	<p>Sign up to our newsletter</p>
                    
                    <input type="text" value="" name="" placeholder="Enter email address here" class="text-box" />
                    <input type="submit" value="Sign Up"  class="submit-btn" />
                </div>
            
    	
        	</div><!-- footer-section end here -->
        </div><!-- page-wrapper end here -->
    </div><!-- footer-wrapper end here -->
    
</div><!--site-wrapper end here-->
<script>
$(document).ready(function(){
  var sections = $('.summry-wrapper')
  , nav = $('.nav')
  , nav_height = nav.outerHeight();
 
$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
 
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
 
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a').removeClass('active');
      sections.removeClass('active');
 
      $(this).addClass('active');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});
nav.find('a').on('click', function () {
  var $el = $(this)
    , id = $el.attr('href');
 
  $('html, body').animate({
    scrollTop: $(id).offset().top - nav_height
  }, 500);
 
  return false;
});
});
</script>
</body>
</html>
