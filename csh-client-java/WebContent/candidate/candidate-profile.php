
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<!-- Specific Page Data -->
<!-- End of Data -->
<head>
<meta charset="utf-8" />
<title>Form Elements - Responsive Multipurpose Admin Templates |
	Vendroid</title>
<meta name="keywords" content="HTML5 Template, CSS3, Mega Menu, Admin Template, Elegant HTML Theme, Vendroid" />
<meta name="description" content="Form Elements - Responsive Admin HTML Template">
<meta name="author" content="Venmond">
<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="img/ico/favicon.png">
<!-- CSS -->
<!-- Bootstrap & FontAwesome & Entypo CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
<link href="css/font-entypo.css" rel="stylesheet" type="text/css">
<!-- Fonts CSS -->
<link href="css/fonts.css" rel="stylesheet" type="text/css">
<!-- Plugin CSS -->
<link href="plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">
<link href="plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
<link href="plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
<link href="plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">
<link href="plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css">
<link href="plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
<link href="plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
<link href="plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">
<link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">
<link href="plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<link href="plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">
<!-- Specific CSS -->
<link href="plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css">
<link href="plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css">
<link href="plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet" type="text/css">
<!-- Theme CSS -->
<link href="css/theme.min.css" rel="stylesheet" type="text/css">
<link href="css/chrome.css" rel="stylesheet" type="text/chrome">
<!-- chrome only css -->
<!-- Responsive CSS -->
<link href="css/theme-responsive.min.css" rel="stylesheet" type="text/css">
<!-- for specific page in style css -->
<!-- for specific page responsive in style css -->
<!-- Custom CSS -->
<link href="custom/custom.css" rel="stylesheet" type="text/css">
<!-- Head SCRIPTS -->
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/mobile-detect.min.js"></script>
<script type="text/javascript" src="js/mobile-detect-modernizr.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
</head>

<body id="forms"
	class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix"
	data-active="forms " data-smooth-scrolling="1">
	<div class="vd_body">
		<!-- Header Start -->
  <!-- Header Start -->
  <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
          <div class="vd_panel-header">
          	<div class="logo">
            	<a href="index.html"><img alt="logo" src="img/logo.png"></a>
            </div>
            <!-- logo -->
            <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="<strong>Minimize Left Navigation</strong><br/>Toggle navigation size to medium or small size. You can set both button or one button only. See full option at documentation." data-step=1>
            		                	<span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Medium Nav Toggle" data-action="nav-left-medium">
	                    <i class="fa fa-bars"></i>
                    </span>
                                		                    
                	<span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Small Nav Toggle" data-action="nav-left-small">
	                    <i class="fa fa-ellipsis-v"></i>
                    </span> 
                                       
            </div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
                                 
                        <span class="menu" data-action="toggle-navbar-left">
                            <i class="fa fa-ellipsis-v"></i>
                        </span>  
                            
                              
            </div>
            <div class="vd_panel-menu visible-sm visible-xs">
                	<span class="menu visible-xs" data-action="submenu">
	                    <i class="fa fa-bars"></i>
                    </span>        
                          
                        <span class="menu visible-sm visible-xs" data-action="toggle-navbar-right">
                            <i class="fa fa-comments"></i>
                        </span>                   
                   	 
            </div>                                     
            <!-- vd_panel-menu -->
          </div>
          <!-- vd_panel-header -->
            
          </div>    
          <div class="vd_container">
          	<div class="row">
            	<div class="col-sm-5 col-xs-12">
            		
<div class="vd_menu-search">
  <form id="search-box" method="post" action="#">
    <input type="text" name="search" class="vd_menu-search-text width-60" placeholder="Search">
    <div class="vd_menu-search-category"> <span data-action="click-trigger"> <span class="separator"></span> <span class="text">Category</span> <span class="icon"> <i class="fa fa-caret-down"></i></span> </span>
      <div class="vd_mega-menu-content width-xs-2 center-xs-2 right-sm" data-action="click-target">
        <div class="child-menu">
          <div class="content-list content-menu content">
            <ul class="list-wrapper">
              <li>
                <label>
                  <input type="checkbox" value="all-files">
                  <span>All Files</span></label>
              </li>
              <li>
                <label>
                  <input type="checkbox" value="photos">
                  <span>Photos</span></label>
              </li>
              <li>
                <label>
                  <input type="checkbox" value="illustrations">
                  <span>Illustrations</span></label>
              </li>
              <li>
                <label>
                  <input type="checkbox" value="video">
                  <span>Video</span></label>
              </li>
              <li>
                <label>
                  <input type="checkbox" value="audio">
                  <span>Audio</span></label>
              </li>
              <li>
                <label>
                  <input type="checkbox" value="flash">
                  <span>Flash</span></label>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <span class="vd_menu-search-submit"><i class="fa fa-search"></i> </span>
  </form>
</div>
                </div>
                <div class="col-sm-7 col-xs-12">
              		<div class="vd_mega-menu-wrapper">
                    	<div class="vd_mega-menu pull-right">
            				<ul class="mega-ul">
   
    <li id="top-menu-1" class="one-icon mega-li"> 
      <a href="index.html" class="mega-link" data-action="click-trigger">
    	<span class="mega-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span> 
		<span class="badge vd_bg-red">3</span>        
      </a>
      <div class="vd_mega-menu-content width-xs-3 width-sm-4 width-md-5 width-lg-4 right-xs left-sm" data-action="click-target">
        <div class="child-menu">  
           <div class="title"> 
           	  <h4 class="padding-top-10"> Your Cart</h4>
           </div>                 
		   <div class="content-list content-image">
           	   <div  data-rel="scroll">	
               <ul class="list-wrapper pd-lr-10">
                    <li> 
                    <a href="mycart.php">
                    		<div class="menu-icon"><i class="glyphicon glyphicon-ok"></i></div> 
                            <div class="menu-text">
								<div class="service">Service: <span>Expert Review</span></div>
								<div class="candidate-name">Candidate Name: <span>Pallav Kakkar</span></div>
								<div class="project-name">Project Name: <span>Web Development</span></div>
								<div class="text-right vd_green">Cost: <span>$500</span></div>
                            </div> 
                    </a>
                    </li>
                    <li class="warning"> 
                     <a href="mycart.php">

                    		<div class="menu-icon"><i class="glyphicon glyphicon-ok"></i></div> 
                            <div class="menu-text">
								<div class="service">Service: <span>Expert Review</span></div>
								<div class="candidate-name">Candidate Name: <span>Pallav Kakkar</span></div>
								<div class="project-name">Project Name: <span>Web Development</span></div>
								<div class="text-right vd_green">Cost: <span>$500</span></div>
                            </div> 
                     </a>
                    </li>    
                    <li> 
                      <a href="mycart.php">

                    		<div class="menu-icon"><i class="glyphicon glyphicon-ok"></i></div> 
                            <div class="menu-text">
								<div class="service">Service: <span>Expert Review</span></div>
								<div class="candidate-name">Candidate Name: <span>Pallav Kakkar</span></div>
								<div class="project-name">Project Name: <span>Web Development</span></div>
								<div class="text-right vd_green">Cost: <span>$500</span></div>
                            </div> 
                      </a>
                    </li>
					<li>
						<div class="menu-text text-right">
							<h4 class="vd_green">Total: <span>$1500</span></h4>
						</div>
					</li>
               </ul>
               </div>
               <div class="closing text-center" style="">
               		<a href="#">Check Out <i class="fa fa-angle-double-right"></i></a>
               </div>                                                                       
           </div>                              
        </div> <!-- child-menu -->                      
      </div>   <!-- vd_mega-menu-content --> 
    </li>  
    <li id="top-menu-3"  class="one-icon mega-li"> 
      <a href="index.html" class="mega-link" data-action="click-trigger">
    	<span class="mega-icon"><i class="fa fa-globe"></i></span> 
		<span class="badge vd_bg-red">5</span>        
      </a>
      <div class="vd_mega-menu-content  width-xs-3 width-sm-4  center-xs-3 left-sm" data-action="click-target">
        <div class="child-menu">  
           <div class="title"> 
           		<h4 class="padding-top-10"> Your Notifications</h4>
               <div class="vd_panel-menu">
                     <span data-original-title="Notification Setting" data-toggle="tooltip" data-placement="bottom" class="menu">
                        <i class="fa fa-cog"></i>
                    </span>                   
<!--                     <span class="text-menu" data-original-title="Settings" data-toggle="tooltip" data-placement="bottom">
                        Settings
                    </span> -->                                                              
                </div>
           </div>                 
		   <div class="content-list">	
           	   <div  data-rel="scroll">	
               <ul  class="list-wrapper pd-lr-10">
                    <li> <a href="#"> 
                    		<div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div> 
                            <div class="menu-text"> Candidate Pallav Kakkar has completed assessment 
                            	<div class="menu-info"><span class="menu-date">12 Minutes Ago</span></div>
                            </div> 
                    </a> </li>
                    <li> <a href="#"> 
                    		<div class="menu-icon vd_blue"><i class=" fa fa-user"></i></div> 
                            <div class="menu-text">  Stakeholder John Doe has completed review
                            	<div class="menu-info"><span class="menu-date">1 Hour 20 Minutes Ago</span></div>
                            </div> 
                    </a> </li>    
                    <li> <a href="#"> 
                    		<div class="menu-icon vd_red"><i class=" fa fa-cogs"></i></div> 
                            <div class="menu-text">  Expert Review for candidate Pallav Kakkar complete
                            	<div class="menu-info"><span class="menu-date">12 Days Ago</span></div>                            
                            </div> 
                    </a> </li>                                     
                    <li> <a href="#"> 
                    		<div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div> 
                            <div class="menu-text"> Candidate Pallav Kakkar has completed assessment 
                            	<div class="menu-info"><span class="menu-date">12 Minutes Ago</span></div>
                            </div> 
                    </a> </li>
<li> <a href="#"> 
                    		<div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div> 
                            <div class="menu-text"> Candidate Pallav Kakkar has completed assessment 
                            	<div class="menu-info"><span class="menu-date">12 Minutes Ago</span></div>
                            </div> 
                    </a> </li>
					
                    
               </ul>
               </div>
               <div class="closing text-center" style="">
               		<a href="#">See All Notifications <i class="fa fa-angle-double-right"></i></a>
               </div>                                                                       
           </div>                              
        </div> <!-- child-menu -->                      
      </div>   <!-- vd_mega-menu-content -->         
    </li>  
     
    <li id="top-menu-profile" class="profile mega-li"> 
        <a href="#" class="mega-link"  data-action="click-trigger"> 
            <span  class="mega-image">
                <img src="img/avatar/man.jpg" alt="example image" />               
            </span>
            <span class="mega-name">
                Caroline <i class="fa fa-caret-down fa-fw"></i> 
            </span>
        </a> 
      <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
        <div class="child-menu"> 
        	<div class="content-list content-menu">
                <ul class="list-wrapper pd-lr-10">
                    <li> <a href="user-profile.php"> <div class="menu-icon"><i class=" fa fa-user"></i></div> <div class="menu-text">Edit Profile</div> </a> </li>
					<li> <a href="#"> <div class="menu-icon"><i class=" glyphicon glyphicon-barcode"></i></div> <div class="menu-text">Your Orders</div> </a> </li>
					<li> <a href="#"> <div class="menu-icon"><i class=" fa fa-question-circle"></i></div> <div class="menu-text">Help</div> </a> </li>
                    <li class="line"></li>   
                    <li> <a href="#"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>  <div class="menu-text">Sign Out</div> </a> </li>                          
                </ul>
            </div> 
        </div> 
      </div>     
  
    </li>               
       
    <!--<li id="top-menu-settings" class="one-big-icon hidden-xs hidden-sm mega-li" data-intro="<strong>Toggle Right Navigation </strong><br/>On smaller device such as tablet or phone you can click on the middle content to close the right or left navigation." data-step=2 data-position="left"> 
    	<a href="#" class="mega-link"  data-action="toggle-navbar-right"> 
           <span class="mega-icon">
                <i class="fa fa-comments"></i> 
            </span> 
         
			<span class="badge vd_bg-red">8</span>               
        </a>              
       
    </li>-->
	</ul>
<!-- Head menu search form ends -->                         
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
        <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 

  </header>
  <!-- Header Ends -->  <!-- Header Ends -->
		<div class="content">
			<div class="container">
   
   <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
	<div class="navbar-menu clearfix">
        <div class="vd_panel-menu hidden-xs">
            <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom" data-action="expand-all" class="menu" data-intro="<strong>Expand Button</strong><br/>To expand all menu on left navigation menu." data-step=4 >
                <i class="fa fa-sort-amount-asc"></i>
            </span>                   
        </div>
    	<h3 class="menu-title hide-nav-medium hide-nav-small">Welcome!</h3>
        <div class="vd_menu">
        	 <ul>
			 <li>
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-tools"> </i></span> 
            <span class="menu-text">Your Job Invites</span>  
			<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
     	<div class="child-menu"  data-action="click-target">
            <ul>  
                <li>
                   <a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-tools"> </i></span> 
            <span class="menu-text">php Developer</span>  
            <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
					<div class="child-menu"  data-action="click-target">
            <ul>  
                <li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">Job Details</span>  
                    </a>
                </li> 
				<li>
                    <a href="candidate-assessment-vid-step2.php">
						<span class="menu-badge"><span class="badge vd_bg-green"><i class="fa fa-fw fa-check"></i></span></span>
                        <span class="menu-text">Assessment</span>  
                    </a>
                </li>				
            </ul>   
      	</div>
                </li>
				<li>
                   <a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-tools"> </i></span> 
            <span class="menu-text">Web Design</span>  
            <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
					<div class="child-menu"  data-action="click-target">
            <ul>  
                <li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">Job Details</span>  
                    </a>
                </li> 
				<li>
                    <a href="candidate-assessment-vid-step2.php">
						<span class="menu-badge"><span class="badge vd_bg-green"><i class="fa fa-fw fa-check"></i></span></span>
                        <span class="menu-text">Assessment</span>  
                    </a>
                </li>				
            </ul>   
      	</div>
                </li>
				<li>
                   <a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-tools"> </i></span> 
            <span class="menu-text">Java Developer</span>  
            <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
					<div class="child-menu"  data-action="click-target">
            <ul>  
                <li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">Job Details</span>  
                    </a>
                </li> 
				<li>
                    <a href="candidate-assessment-vid-step2.php">
						<span class="menu-badge"><span class="badge vd_bg-green"><i class="fa fa-fw fa-check"></i></span></span>
                        <span class="menu-text">Assessment</span>  
                    </a>
                </li>				
            </ul>   
      	</div>
                </li>
            </ul>   
      	</div>
    </li>    
	<li>
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="fa fa-fw fa-phone "> </i></span> 
            <span class="menu-text">Your Interview Invites</span>  
			<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
     	<div class="child-menu"  data-action="click-target">
            <ul>  
                <li>
                    <a href="javascript:;">
                        <span class="menu-text">Dot Net</span>  
                    </a>
                </li>
				<li>
                    <a href="manage-jobs.php">
                        <span class="menu-text">Nodejs Programmer</span>  
                    </a>
                </li>
            </ul>   
      	</div>
    </li>  
    <li>
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-archive"></i></span> 
            <span class="menu-text">Archived Jobs </span>  
            <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
        
        <div class="child-menu"  data-action="click-target">
            <ul>  
			<li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">Human Resources</span>  
                    </a>
                </li>
				<li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">Project Manager</span>  
                    </a>
                </li>
                
                
                <li>
                    <a href="candidate-assessment-step1.php">
                        <span class="menu-text">System Analyst</span>  
                    </a>
                </li>
			
            </ul>   
      	</div>
     
    </li> 
    <li>
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="icon-tools"> </i></span> 
            <span class="menu-text">Pre Assessments</span>  
			<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
       	</a>
     	<div class="child-menu"  data-action="click-target">
            <ul>  
			<li>
                    <a href="javascript:;">
                        <span class="menu-text">JavaScript Test</span>  
                    </a>
                </li> 
            </ul>   
      	</div>
    </li>
    <!-- <li>
    	<a href="shortlisted-candidates.php" >
        	<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
            <span class="menu-text">Shortlists </span>  
            <span class="menu-badge"><span class="badge vd_bg-yellow">10</span></span>
       	</a>
     
    </li> 
    <li>
    	<a href="javascript:void(0);" data-action="click-trigger">
        	<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
            <span class="menu-text">Discussion </span>  
            <span class="menu-badge"><span class="badge vd_bg-green">5</span></span>
       	</a>
     
    </li> 
    
    <li>
    	<a href="manage-jobs.php" >
        	<span class="menu-icon"><i class="fa fa-dashboard"></i></span> 
            <span class="menu-text">Manage Jobs </span>  
            <span class="menu-badge"><span class="badge vd_bg-green">5</span></span>
       	</a>
     
    </li> -->
 	 
                 
</ul>
<!-- Head menu search form ends -->         </div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>
    <div class="vd_menu vd_navbar-bottom-widget">
        <ul>
            <li>
                <a href="pages-logout.php">
                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>          
                    <span class="menu-text">Logout</span>             
                </a>
                
            </li>
        </ul>
    </div>     
</div>    <!-- Middle Content Start -->
				<!-- Middle Content Start -->

				<div class="vd_content-wrapper">
					<div class="vd_container">
						<div class="vd_content clearfix">
							<div class="vd_head-section clearfix">
								<div class="vd_panel-header">

									<div class="vd_panel-menu hidden-sm hidden-xs"
										data-intro="<strong> Expand Control
										
										</strong><br />To expand content page horizontally,
										vertically, or Both. If you just need one button just simply
										remove the other button code." data-step=5
										data-position="left">
										<div data-action="remove-navbar"
											data-original-title="Remove Navigation Bar Toggle"
											data-toggle="tooltip" data-placement="bottom"
											class="remove-navbar-button menu">
											<i class="fa fa-arrows-h"></i>
										</div>
										<div data-action="remove-header"
											data-original-title="Remove Top Menu Toggle"
											data-toggle="tooltip" data-placement="bottom"
											class="remove-header-button menu">
											<i class="fa fa-arrows-v"></i>
										</div>
										<div data-action="fullscreen"
											data-original-title="Remove Navigation Bar and Top Menu Toggle"
											data-toggle="tooltip" data-placement="bottom"
											class="fullscreen-button menu">
											<i class="glyphicon glyphicon-fullscreen"></i>
										</div>

									</div>

								</div>
							</div>
							<div class="vd_title-section clearfix">
								<div class="vd_panel-header no-subtitle">
									<h1>Candidate Profile Page</h1>
								</div>
							</div>
							<div class="vd_content-section clearfix">
								<div class="row">
									<div class="col-sm-3">
										<div class="panel widget light-widget panel-bd-top">
											<div class="panel-heading no-title"></div>
											<div class="panel-body">
												<div class="text-center vd_info-parent">
													<img alt="example image" src="img/avatar/big.jpg">
												</div>
												<div class="row">
													<div class="col-xs-12">
														<a
															class="btn vd_btn vd_bg-linkedin btn-xs btn-block no-br"><i
															class="fa fa-check-circle append-icon"></i>Change Image</a>
													</div>

												</div>
												<h4 class="font-semibold mgbt-xs-5">Candidate Name</h4>

											</div>
										</div>
										<!-- panel widget -->

									</div>
									<div class="col-sm-9">
										<!-- form wizard start here -->
										<div class="panel widget">

											<div class="panel-body">
												<form class="form-horizontal" action="#" role="form">
													<div id="wizard-2" class="form-wizard">
														<ul>
															<li><a href="#tab21" data-toggle="tab">
																	<div class="menu-icon">1</div> Your Profile
															</a></li>
															<li><a href="#tab22" data-toggle="tab">
																	<div class="menu-icon">2</div> Work Experience
															</a></li>
															<li><a href="#tab23" data-toggle="tab">
																	<div class="menu-icon">3</div> Education
															</a></li>

															<li><a href="#tab25" data-toggle="tab">
																	<div class="menu-icon">5</div> Account Settings
															</a></li>
														</ul>
														<div class="progress progress-striped active">
															<div class="progress-bar progress-bar-info"></div>
														</div>
														<div class="tab-content no-bd ">
															<div class="tab-pane" id="tab21">
																<div class="">
																	<h3 class="mgbt-xs-15 mgtp-40 font-semibold">Your
																		Profile</h3>
																	<div class="row">

																		<div class="form-group">
																			<label class="col-sm-3 control-label">First Name</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm"><span
																					class="help-inline">This field is compulsory </span>
																			</div>
																			<label class="col-sm-2 control-label">Last Name</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm"><span
																					class="help-inline">This field is compulsory </span>
																			</div>
																		</div>
																		<!-- formgroup end here -->
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Current City</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm"><span
																					class="help-inline">This field is compulsory </span>
																			</div>
																			<label class="col-sm-2 control-label">Country</label>
																			<div class="col-sm-3 controls">
																				<select>
																					<option value="AF">Afghanistan</option>
																					<option value="AX">Åland Islands</option>
																					<option value="AL">Albania</option>
																					<option value="DZ">Algeria</option>
																					<option value="AS">American Samoa</option>
																					<option value="AD">Andorra</option>
																					<option value="AO">Angola</option>
																					<option value="AI">Anguilla</option>
																					<option value="AQ">Antarctica</option>
																					<option value="AG">Antigua and Barbuda</option>
																					<option value="AR">Argentina</option>
																					<option value="AM">Armenia</option>
																					<option value="AW">Aruba</option>
																					<option value="AU">Australia</option>
																					<option value="AT">Austria</option>
																					<option value="AZ">Azerbaijan</option>
																					<option value="BS">Bahamas</option>
																					<option value="BH">Bahrain</option>
																					<option value="BD">Bangladesh</option>
																					<option value="BB">Barbados</option>
																					<option value="BY">Belarus</option>
																					<option value="BE">Belgium</option>
																					<option value="BZ">Belize</option>
																					<option value="BJ">Benin</option>
																					<option value="BM">Bermuda</option>
																					<option value="BT">Bhutan</option>
																					<option value="BO">Bolivia, Plurinational State of</option>
																					<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
																					<option value="BA">Bosnia and Herzegovina</option>
																					<option value="BW">Botswana</option>
																					<option value="BV">Bouvet Island</option>
																					<option value="BR">Brazil</option>
																					<option value="IO">British Indian Ocean Territory</option>
																					<option value="BN">Brunei Darussalam</option>
																					<option value="BG">Bulgaria</option>
																					<option value="BF">Burkina Faso</option>
																					<option value="BI">Burundi</option>
																					<option value="KH">Cambodia</option>
																					<option value="CM">Cameroon</option>
																					<option value="CA">Canada</option>
																					<option value="CV">Cape Verde</option>
																					<option value="KY">Cayman Islands</option>
																					<option value="CF">Central African Republic</option>
																					<option value="TD">Chad</option>
																					<option value="CL">Chile</option>
																					<option value="CN">China</option>
																					<option value="CX">Christmas Island</option>
																					<option value="CC">Cocos (Keeling) Islands</option>
																					<option value="CO">Colombia</option>
																					<option value="KM">Comoros</option>
																					<option value="CG">Congo</option>
																					<option value="CD">Congo, the Democratic Republic
																						of the</option>
																					<option value="CK">Cook Islands</option>
																					<option value="CR">Costa Rica</option>
																					<option value="CI">Côte d'Ivoire</option>
																					<option value="HR">Croatia</option>
																					<option value="CU">Cuba</option>
																					<option value="CW">Curaçao</option>
																					<option value="CY">Cyprus</option>
																					<option value="CZ">Czech Republic</option>
																					<option value="DK">Denmark</option>
																					<option value="DJ">Djibouti</option>
																					<option value="DM">Dominica</option>
																					<option value="DO">Dominican Republic</option>
																					<option value="EC">Ecuador</option>
																					<option value="EG">Egypt</option>
																					<option value="SV">El Salvador</option>
																					<option value="GQ">Equatorial Guinea</option>
																					<option value="ER">Eritrea</option>
																					<option value="EE">Estonia</option>
																					<option value="ET">Ethiopia</option>
																					<option value="FK">Falkland Islands (Malvinas)</option>
																					<option value="FO">Faroe Islands</option>
																					<option value="FJ">Fiji</option>
																					<option value="FI">Finland</option>
																					<option value="FR">France</option>
																					<option value="GF">French Guiana</option>
																					<option value="PF">French Polynesia</option>
																					<option value="TF">French Southern Territories</option>
																					<option value="GA">Gabon</option>
																					<option value="GM">Gambia</option>
																					<option value="GE">Georgia</option>
																					<option value="DE">Germany</option>
																					<option value="GH">Ghana</option>
																					<option value="GI">Gibraltar</option>
																					<option value="GR">Greece</option>
																					<option value="GL">Greenland</option>
																					<option value="GD">Grenada</option>
																					<option value="GP">Guadeloupe</option>
																					<option value="GU">Guam</option>
																					<option value="GT">Guatemala</option>
																					<option value="GG">Guernsey</option>
																					<option value="GN">Guinea</option>
																					<option value="GW">Guinea-Bissau</option>
																					<option value="GY">Guyana</option>
																					<option value="HT">Haiti</option>
																					<option value="HM">Heard Island and McDonald
																						Islands</option>
																					<option value="VA">Holy See (Vatican City State)</option>
																					<option value="HN">Honduras</option>
																					<option value="HK">Hong Kong</option>
																					<option value="HU">Hungary</option>
																					<option value="IS">Iceland</option>
																					<option value="IN">India</option>
																					<option value="ID">Indonesia</option>
																					<option value="IR">Iran, Islamic Republic of</option>
																					<option value="IQ">Iraq</option>
																					<option value="IE">Ireland</option>
																					<option value="IM">Isle of Man</option>
																					<option value="IL">Israel</option>
																					<option value="IT">Italy</option>
																					<option value="JM">Jamaica</option>
																					<option value="JP">Japan</option>
																					<option value="JE">Jersey</option>
																					<option value="JO">Jordan</option>
																					<option value="KZ">Kazakhstan</option>
																					<option value="KE">Kenya</option>
																					<option value="KI">Kiribati</option>
																					<option value="KP">Korea, Democratic People's
																						Republic of</option>
																					<option value="KR">Korea, Republic of</option>
																					<option value="KW">Kuwait</option>
																					<option value="KG">Kyrgyzstan</option>
																					<option value="LA">Lao People's Democratic Republic</option>
																					<option value="LV">Latvia</option>
																					<option value="LB">Lebanon</option>
																					<option value="LS">Lesotho</option>
																					<option value="LR">Liberia</option>
																					<option value="LY">Libya</option>
																					<option value="LI">Liechtenstein</option>
																					<option value="LT">Lithuania</option>
																					<option value="LU">Luxembourg</option>
																					<option value="MO">Macao</option>
																					<option value="MK">Macedonia, the former Yugoslav
																						Republic of</option>
																					<option value="MG">Madagascar</option>
																					<option value="MW">Malawi</option>
																					<option value="MY">Malaysia</option>
																					<option value="MV">Maldives</option>
																					<option value="ML">Mali</option>
																					<option value="MT">Malta</option>
																					<option value="MH">Marshall Islands</option>
																					<option value="MQ">Martinique</option>
																					<option value="MR">Mauritania</option>
																					<option value="MU">Mauritius</option>
																					<option value="YT">Mayotte</option>
																					<option value="MX">Mexico</option>
																					<option value="FM">Micronesia, Federated States of</option>
																					<option value="MD">Moldova, Republic of</option>
																					<option value="MC">Monaco</option>
																					<option value="MN">Mongolia</option>
																					<option value="ME">Montenegro</option>
																					<option value="MS">Montserrat</option>
																					<option value="MA">Morocco</option>
																					<option value="MZ">Mozambique</option>
																					<option value="MM">Myanmar</option>
																					<option value="NA">Namibia</option>
																					<option value="NR">Nauru</option>
																					<option value="NP">Nepal</option>
																					<option value="NL">Netherlands</option>
																					<option value="NC">New Caledonia</option>
																					<option value="NZ">New Zealand</option>
																					<option value="NI">Nicaragua</option>
																					<option value="NE">Niger</option>
																					<option value="NG">Nigeria</option>
																					<option value="NU">Niue</option>
																					<option value="NF">Norfolk Island</option>
																					<option value="MP">Northern Mariana Islands</option>
																					<option value="NO">Norway</option>
																					<option value="OM">Oman</option>
																					<option value="PK">Pakistan</option>
																					<option value="PW">Palau</option>
																					<option value="PS">Palestinian Territory, Occupied</option>
																					<option value="PA">Panama</option>
																					<option value="PG">Papua New Guinea</option>
																					<option value="PY">Paraguay</option>
																					<option value="PE">Peru</option>
																					<option value="PH">Philippines</option>
																					<option value="PN">Pitcairn</option>
																					<option value="PL">Poland</option>
																					<option value="PT">Portugal</option>
																					<option value="PR">Puerto Rico</option>
																					<option value="QA">Qatar</option>
																					<option value="RE">Réunion</option>
																					<option value="RO">Romania</option>
																					<option value="RU">Russian Federation</option>
																					<option value="RW">Rwanda</option>
																					<option value="BL">Saint Barthélemy</option>
																					<option value="SH">Saint Helena, Ascension and
																						Tristan da Cunha</option>
																					<option value="KN">Saint Kitts and Nevis</option>
																					<option value="LC">Saint Lucia</option>
																					<option value="MF">Saint Martin (French part)</option>
																					<option value="PM">Saint Pierre and Miquelon</option>
																					<option value="VC">Saint Vincent and the Grenadines</option>
																					<option value="WS">Samoa</option>
																					<option value="SM">San Marino</option>
																					<option value="ST">Sao Tome and Principe</option>
																					<option value="SA">Saudi Arabia</option>
																					<option value="SN">Senegal</option>
																					<option value="RS">Serbia</option>
																					<option value="SC">Seychelles</option>
																					<option value="SL">Sierra Leone</option>
																					<option value="SG">Singapore</option>
																					<option value="SX">Sint Maarten (Dutch part)</option>
																					<option value="SK">Slovakia</option>
																					<option value="SI">Slovenia</option>
																					<option value="SB">Solomon Islands</option>
																					<option value="SO">Somalia</option>
																					<option value="ZA">South Africa</option>
																					<option value="GS">South Georgia and the South
																						Sandwich Islands</option>
																					<option value="SS">South Sudan</option>
																					<option value="ES">Spain</option>
																					<option value="LK">Sri Lanka</option>
																					<option value="SD">Sudan</option>
																					<option value="SR">Suriname</option>
																					<option value="SJ">Svalbard and Jan Mayen</option>
																					<option value="SZ">Swaziland</option>
																					<option value="SE">Sweden</option>
																					<option value="CH">Switzerland</option>
																					<option value="SY">Syrian Arab Republic</option>
																					<option value="TW">Taiwan, Province of China</option>
																					<option value="TJ">Tajikistan</option>
																					<option value="TZ">Tanzania, United Republic of</option>
																					<option value="TH">Thailand</option>
																					<option value="TL">Timor-Leste</option>
																					<option value="TG">Togo</option>
																					<option value="TK">Tokelau</option>
																					<option value="TO">Tonga</option>
																					<option value="TT">Trinidad and Tobago</option>
																					<option value="TN">Tunisia</option>
																					<option value="TR">Turkey</option>
																					<option value="TM">Turkmenistan</option>
																					<option value="TC">Turks and Caicos Islands</option>
																					<option value="TV">Tuvalu</option>
																					<option value="UG">Uganda</option>
																					<option value="UA">Ukraine</option>
																					<option value="AE">United Arab Emirates</option>
																					<option value="GB">United Kingdom</option>
																					<option value="US">United States</option>
																					<option value="UM">United States Minor Outlying
																						Islands</option>
																					<option value="UY">Uruguay</option>
																					<option value="UZ">Uzbekistan</option>
																					<option value="VU">Vanuatu</option>
																					<option value="VE">Venezuela, Bolivarian Republic
																						of</option>

																					<option value="VN">Viet Nam</option>
																					<option value="VG">Virgin Islands, British</option>
																					<option value="VI">Virgin Islands, U.S.</option>
																					<option value="WF">Wallis and Futuna</option>
																					<option value="EH">Western Sahara</option>
																					<option value="YE">Yemen</option>
																					<option value="ZM">Zambia</option>
																					<option value="ZW">Zimbabwe</option>
																				</select> <span class="help-inline">This field is
																					compulsory </span>
																			</div>
																		</div>
																		<!-- formgroup end here -->
																		<div class="form-group">
																			<label class="col-sm-3 control-label">A brief summary
																				about you</label>
																			<div class="col-sm-8 controls">
																				<textarea class="" rows="3"></textarea>
																			</div>

																		</div>
																		<!-- formgroup end here -->
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Skills</label>
																			<div class="col-sm-8 controls">
																				<input id="input-autocomplete" type="text"
																					class="tags" value="Ruby,Rails,Java" />
																			</div>
																		</div>
																		<!-- formgroup end here -->
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Email Address</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm">
																			</div>
																			<label class="col-sm-2 control-label"> Phone Number</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Linkedin URL</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm">
																			</div>
																			<label class="col-sm-2 control-label">Address</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Expected Salary</label>
																			<div class="col-sm-3 controls">
																				<select>
																					<option>$0 - $40,000</option>
																					<option>$40,000 - $60,000</option>
																					<option>$60,000 - $80,000</option>
																					<option>$80,000 - $100,000</option>
																					<option>$100,000 - $150,000</option>
																					<option>Other</option>
																				</select>
																			</div>
																			<label class="col-sm-2 control-label">Other</label>
																			<div class="col-sm-3 controls">
																				<input type="text" class="input-border-btm">
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-3 control-label">Employment
																				Status</label>
																			<div class="col-sm-9 controls">
																				<div class="vd_radio radio-info">
																					<input type="radio" name="optionsRadios2"
																						id="optionsRadios3" value="option3" checked><label
																						for="optionsRadios3"> Open to opportunities </label>
																					<input type="radio" name="optionsRadios2"
																						id="optionsRadios4" value="option4"><label
																						for="optionsRadios4"> I'm not interested in new
																						opportunities </label> <input type="radio"
																						name="optionsRadios2" id="optionsRadios5"
																						value="option5"><label for="optionsRadios5">
																						Approach me only for </label>
																				</div>
																			</div>
																			<div
																				class="col-sm-9 col-md-offset-3 controls padding-left-0">
																				<div class="vd_checkbox checkbox-info">
																					<div class="col-md-4">
																						<input type="checkbox" value="1" id="checkbox-3"><label
																							for="checkbox-3"> Companies </label>
																					</div>
																					<div class="col-md-6">
																						<input type="checkbox" value="2" id="checkbox-4"><label
																							for="checkbox-4"> Matching Skills </label>
																					</div>
																					<div class="col-md-6">
																						<input type="checkbox" value="3" id="checkbox-5"><label
																							for="checkbox-5"> Expected Salary </label>
																					</div>
																				</div>
																			</div>
																			<div class="form-group">
																				<label class="col-sm-3 control-label">Upload Your CV</label>

																				<div class="col-sm-4 controls">
																					<input type="file"><span class="help-inline">This
																						field is compulsory </span>
																				</div>
																				<label class="col-sm-3 control-label">Last Update: <span>10-05-15</span></label>
																			</div>
																		</div>
																		<div class="col-md-4 col-md-offset-5">
																			<button
																				class="btn vd_btn vd_bg-twitter btn-sm btn-block"
																				type="button">Preview Your Profile</button>
																		</div>

																	</div>

																</div>
																<!-- pd-20 -->
															</div>
															<div class="tab-pane" id="tab22">

																<h3 class="mgbt-xs-15 mgtp-40 font-semibold">Work
																	Experience</h3>
																<div class="form-group">
																	<label class="col-sm-3 control-label">Company Name</label>
																	<div class="col-sm-3 controls">
																		<input type="text" class="input-border-btm">
																	</div>
																	<label class="col-sm-2 control-label">Designation</label>
																	<div class="col-sm-3 controls">
																		<input type="text" class="input-border-btm">
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-3 control-label">Duration</label>
																	<div class="col-sm-3 controls">
																		<input type="text" placeholder="From" class="datepicker-normal">
																	</div>
                                                                    
																	<div class="col-sm-3 controls">
																		<input type="text" placeholder="To" class="datepicker-normal">
																	</div>
																</div>
																<!-- formgroup end here -->
																<div class="form-group">
																	<label class="col-sm-3 control-label">Your key
																		responsibilities</label>
																	<div class="col-sm-8 controls">
																		<textarea class="" rows="3"></textarea>
																	</div>

																</div>
																<div class="col-md-4 col-md-offset-5">
																	<button
																		class="btn vd_btn vd_bg-twitter btn-sm btn-block"
																		type="button">Add More</button>
																</div>
																<div style="clear: both"></div>
															</div>
															<div class="tab-pane" id="tab23">
																<h3 class="mgbt-xs-15 mgtp-40 font-semibold">Education</h3>
																<div class="row">
                                                                	<div class="form-group">
																		<label class="col-sm-3 control-label">Qualification</label>
																		<div class="col-sm-3 controls">
																			<input type="text" class="input-border-btm">
																		</div>
																		<label class="col-sm-2 control-label">Institution</label>
																		<div class="col-sm-3 controls">
																			<input type="text" class="input-border-btm">
																		</div>
																	</div>
                                                                <div class="form-group">
																	<label class="col-sm-3 control-label">Year</label>
																	<div class="col-sm-3 controls">
																		<input type="text" placeholder="From" class="datepicker-normal">
																	</div>
                                                                   
																	<div class="col-sm-3 controls">
																		<input type="text" placeholder="To" class="datepicker-normal">
																	</div>
																</div>
                                                                
                                                                
																	
																	<!-- formgroup end here -->
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Highlights</label>
																		<div class="col-sm-8 controls">
																			<textarea class="" rows="3"></textarea>
																		</div>

																	</div>
																	<div class="col-md-4 col-md-offset-5">
																		<button type="button"
																			class="btn vd_btn vd_bg-twitter btn-sm btn-block">Add
																			More</button>
																	</div>

																	<!-- formgroup end here -->
																</div>
															</div>

															<div class="tab-pane" id="tab25">
																<h3 class="mgbt-xs-15 mgtp-40 font-semibold">Account
																	Settings</h3>
																<div class="row">


																	<div class="form-group">
																		<label class="col-sm-3 control-label">Publish Profile</label>
																		<div class="col-sm-3 controls">
																			<input type="checkbox" data-size="small"
																				data-rel="switch" data-wrapper-class="inverse"
																				data-on-text="Yes" data-off-text="No" checked>
																			<div class="tool-tip">
																				<a class="blue" data-placement="top"
																					data-toggle="tooltip" data-original-title="view"> <i
																					class="fa fa-fw fa-question"></i>
																				</a>
																			</div>

																		</div>
																		<label class="col-sm-2 control-label">Make Profile
																			Public</label>
																		<div class="col-sm-3 controls">
																			<input type="checkbox" data-size="small"
																				data-rel="switch" data-wrapper-class="inverse"
																				data-on-text="Yes" data-off-text="No" checked>
																			<div class="tool-tip">
																				<a class="blue" data-placement="top"
																					data-toggle="tooltip" data-original-title="view"> <i
																					class="fa fa-fw fa-question"></i>
																				</a>
																			</div>
																		</div>
																	</div>
																	<!-- form-group end here -->
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Set Profile URL</label>
																		<div class="col-sm-4 controls">
																			<input type="text" class="input-border-btm">
																		</div>
																		<div class="col-sm-4 controls">
																			.crowdsourcehire.com
																			<div class="tool-tip">
																				<a class="blue" data-placement="top"
																					data-toggle="tooltip" data-original-title="view"> <i
																					class="fa fa-fw fa-question"></i>
																				</a>
																			</div>
																		</div>

																	</div>
																	<!-- form-group end here -->
																	<div class="margin-left-14">
																		<h4>Change password</h4>
																	</div>
																	<div class="form-group">
																		<label class="col-sm-3 control-label">Current password</label>
																		<div class="col-sm-3 controls">
																			<input type="text" class="input-border-btm">
																		</div>
																		<label class="col-sm-2 control-label">Confirm password</label>
																		<div class="col-sm-3 controls">
																			<input type="text" class="input-border-btm">
																		</div>
																	</div>
																	<!-- form-group end here -->

																	<!-- form-group end here -->
																	<div class="form-group">
																		<div class="col-sm-12 pull-right">
																			<div class="col-md-offset-5">
																				<button class="btn vd_btn vd_bg-green ">Save</button>
																				<button class="btn vd_btn vd_bg-googleplus ">Discard</button>

																			</div>
																		</div>
																	</div>
																	<!-- form-group end here -->

																</div>

															</div>
															<div class="form-actions-condensed wizard">
																<div class="row mgbt-xs-0">
																	<div class="col-sm-9 col-sm-offset-2">
																		<a class="btn vd_btn prev" href="javascript:void(0);"><span
																			class="menu-icon"><i
																				class="fa fa-fw fa-chevron-circle-left"></i></span>
																			Previous</a> <a class="btn vd_btn next"
																			href="javascript:void(0);">Next <span
																			class="menu-icon"><i
																				class="fa fa-fw fa-chevron-circle-right"></i></span></a>
																		<a class="btn vd_btn vd_bg-green finish"
																			href="javascript:void(0);"><span class="menu-icon"><i
																				class="fa fa-fw fa-check"></i></span> Finish</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- Panel Widget -->

										<!-- form wizard end here -->


									</div>
								</div>
								<!-- row -->

							</div>
							<!-- .vd_content-section -->

						</div>
						<!-- .vd_content -->
					</div>
					<!-- .vd_container -->
				</div>
				<!-- .vd_content-wrapper -->

				<!-- Middle Content End -->

			</div>
			<!-- .container -->
		</div>
		<!-- .content -->

		<!-- Footer Start -->
		<footer class="footer-1" id="footer">
			<div class="vd_bottom ">
				<div class="container">
					<div class="row">
						<div class=" col-xs-12">
							<div class="copyright">Copyright &copy;2014 Venmond Inc. All
								Rights Reserved</div>
						</div>
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
		</footer>
		<!-- Footer END -->
	</div>

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
	<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>
	<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
	<script type="text/javascript" src="js/theme.js"></script>
	<script type="text/javascript" src="custom/custom.js"></script>
	<!-- Specific Page Scripts Put Here -->
	<script type="text/javascript" src='plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js'></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#input-autocomplete').tagsInput({
				width: 'auto',
				//autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
				autocomplete_url:'templates/files/fake_json_endpoint.html', // jquery ui autocomplete requires a json endpoint
		/*		autocomplete:{
				source: function(request, response) {
				  $.ajax({
					 url: "templates/files/fake_json_endpoint.html",
					 dataType: "json",
					 data: {
						postalcode_startsWith: request.term
					 },
					 success: function(data) {
						response( $.map( data.postalCodes, function( item ) {
										return {
											label: item.countryCode + "-" + item.placeName,
											value: item.postalCode
										}
									}));
					 }
				  })	
				}} */
			});
			$( ".datepicker-normal" ).datepicker({ dateFormat: 'dd M yy'});
	
			$('#wizard-2').bootstrapWizard({
				'tabClass': 'nav nav-pills nav-justified',
				'nextSelector': '.wizard .next',
				'previousSelector': '.wizard .prev',
				'onTabShow' :  function(tab, navigation, index){
					$('#wizard-2 .finish').hide();
					$('#wizard-2 .next').show();
					if ($('#wizard-2 .nav li:last-child').hasClass('active')){
						$('#wizard-2 .next').hide();
						$('#wizard-2 .finish').show();
					}
					var $total = navigation.find('li').length;
					var $current = index+1;
					var $percent = ($current/$total) * 100;
					$('#wizard-2').find('.progress-bar').css({width:$percent+'%'});			
				},
				'onTabClick': function(tab, navigation, index) {
					return false;		
				},
				'onNext': function(){
					scrollTo('#wizard-2',-100);
				},
				'onPrevious': function(){
					scrollTo('#wizard-2',-100);
				}		
			});	
		});
	</script>
	<!-- Specific Page Scripts END -->

</body>
</html>
