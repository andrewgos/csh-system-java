<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" rel="stylesheet" />
  <link href="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.css" rel="stylesheet" />
	  <link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" rel="stylesheet" />
	
			  <link href="css/bootstrap-modal.css" rel="stylesheet" />
			  

<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link rel="stylesheet" type="text/css" href="css/style.css"/>
<link rel="stylesheet" type="text/css" href="css/slider.css"/>
<link rel="stylesheet" type="text/css" href="css/media-query.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			  <script type="text/javascript" src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
    <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap.js"></script>
    <script src="js/bootstrap-modalmanager.js"></script>
    <script src="js/bootstrap-modal.js"></script>
<script>
$(document).ready(function(){
    $(".menu-btn").click(function(){
        $(".mobile-menu").toggle(300);
    });
var deviceheight = $( window ).height();
$(".parallax").css("height",deviceheight)
});
</script>

<script>
$(function(){
$(".showpara a").click(function(){
		$(".showpara").hide();
		$(".hiddenpara").show();
	});
	
$(".hiddenpara a").click(function(){
		$(".hiddenpara").hide();
		$(".showpara").show();
	});
	});
</script>
 
<title>CrowdSourceHire Candidate Profile - <?php echo $user_first_name.' '.$user_sur_name ?></title>
<base href="<?php echo WEB_SITE_URL ?>">
<!--blog-slider js start here-->
       
          <script type="text/javascript" src="<?php echo $site_path ?>js/jquery.glide.min.js"></script>
          
          
                 <script type="text/javascript">
$(function(){
  $('.slider').glide({
    autoplay:0,
    hoverpause: true, // set to false for nonstop rotate
    arrowRightText: '',
    arrowLeftText: ''
  });
});
</script>
<!--blog-slider js END here-->

<!-- sticky js start here -->
<script src="<?php echo $site_path ?>js/classie.js"></script>
<script>
    function init() {
        window.addEventListener('scroll', function(e){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 545,
                header = document.querySelector(".navbar-wrapper");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init(1000);
</script>

<!-- sticky js end here -->

<script src="<?php echo $site_path2 ?>/pro/circles.min.js"></script>
<script src="<?php echo $site_path2 ?>/pro/highcharts.js"></script>
<script src="<?php echo $site_path2 ?>/pro/highcharts-more.js"></script>
<script src="<?php echo $site_path2 ?>/pro/exporting.js"></script>
<script>

$(function () {

    var avg1=parseInt("<?php if($row_ar_array[0]['average']!='') { echo $row_ar_array[0]['average']; } else { echo 0; } ?>")*10/100;
    var avg2=parseInt("<?php if($row_ar_array[1]['average']!='') { echo $row_ar_array[1]['average']; } else { echo 0; } ?>")*10/100;
    var avg3=parseInt("<?php if($row_ar_array[2]['average']!='') { echo $row_ar_array[2]['average']; } else { echo 0; } ?>")*10/100;
    var avg4=parseInt("<?php if($row_ar_array[3]['average']!='') { echo $row_ar_array[3]['average']; } else { echo 0; } ?>")*10/100;
    var avg5=parseInt("<?php if($row_ar_array[4]['average']!='') { echo $row_ar_array[4]['average']; } else { echo 0; } ?>")*10/100;
    var avg6=parseInt("<?php if($row_ar_array[5]['average']!='') { echo $row_ar_array[5]['average']; } else { echo 0; } ?>")*10/100;
    
    var tavg1=parseInt("<?php if($row_tar_array[0]['rating']!='') { echo $row_tar_array[0]['rating']; } else { echo 0; } ?>")*10/100;
    var tavg2=parseInt("<?php if($row_tar_array[1]['rating']!='') { echo $row_tar_array[1]['rating']; } else { echo 0; } ?>")*10/100;
    var tavg3=parseInt("<?php if($row_tar_array[2]['rating']!='') { echo $row_tar_array[2]['rating']; } else { echo 0; } ?>")*10/100;
    var tavg4=parseInt("<?php if($row_tar_array[3]['rating']!='') { echo $row_tar_array[3]['rating']; } else { echo 0; } ?>")*10/100;
    var tavg5=parseInt("<?php if($row_tar_array[4]['rating']!='') { echo $row_tar_array[4]['rating']; } else { echo 0; } ?>")*10/100;
    var tavg6=parseInt("<?php if($row_tar_array[5]['rating']!='') { echo $row_tar_array[5]['rating']; } else { echo 0; } ?>")*10/100;

   // var categories= ['Sales', 'Marketing', 'Development', 'Customer Support','Test','DES'];
 
	 var tablecon='<ul class="skills-list"> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[0]['rating_cat_describtion']; ?></h3><a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc1);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg1+'/10' + '<br/> Percentile Score: <?php echo round($perc1);?></span> </a> </div> <p><?php echo $row_ar_array[0]['ref_comment']; ?></p> </li> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[1]['rating_cat_describtion']; ?></h3> <a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc2);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg2+'/10' + '<br/> Percentile Score: <?php echo round($perc2);?></span> </a>> </div> <p><?php echo $row_ar_array[1]['ref_comment']; ?></p> </li> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[2]['rating_cat_describtion']; ?></h3> <a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc3);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg3+'/10' + '<br/> Percentile Score: <?php echo round($perc3);?></span> </a></div> <p><?php echo $row_ar_array[2]['ref_comment']; ?></p> </li> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[3]['rating_cat_describtion']; ?></h3> <a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc4);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg4+'/10' + '<br/> Percentile Score: <?php echo round($perc4);?></span> </a> </div> <p><?php echo $row_ar_array[3]['ref_comment']; ?></p> </li> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[4]['rating_cat_describtion']; ?></h3> <a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc5);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg5+'/10' + '<br/> Percentile Score: <?php echo round($perc5);?></span> </a></div> <p><?php echo $row_ar_array[4]['ref_comment']; ?></p> </li> <li> <div class="progress-bar-sec"> <h3><?php echo $row_ar_array[5]['rating_cat_describtion']; ?></h3> <a href="#" class="tooltip">  <div class="progress-bar"><div style="width:<?php echo round($perc6);?>%" class="progress-bar-01"></div></div><span> <img class="callout" src="<?php echo $site_path ?>images/callout.gif" />Candidate Score: ' + tavg6+'/10' + '<br/> Percentile Score: <?php echo round($perc6);?></span> </a></div> <p><?php echo $row_ar_array[5]['ref_comment']; ?></p> </li> </ul>';
	 
     $('.right-skills').html(tablecon);   
	 
		$('#container-chart').highcharts({

      /**/

        chart: {
            polar: true,
            options3d: {
            enabled: true,
                    alpha: 50,
                    beta: 0,
                },
                width: 419,
               
                height: 361
        },

        title: {
            text: ''
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {

            tickInterval: 60,
            min: 0,
            max: 360,
            labels: {
                formatter: function () {
                    if(this.value == 0){
                        return '<?php echo $row_ar_array[0]['rating_cat_describtion'];?>';
                    }else if(this.value == 60){
                        return '<?php echo $row_ar_array[1]['rating_cat_describtion'];?>';
                    }else if(this.value == 120){
                        return '<?php echo $row_ar_array[2]['rating_cat_describtion'];?>';
                    }else if(this.value == 180){
                        return '<small><?php echo $row_ar_array[3]['rating_cat_describtion'];?></small>';
                    }else if(this.value == 240){
                        return '<?php echo $row_ar_array[4]['rating_cat_describtion'];?>';
                    }else if(this.value == 300){
                        return '<?php echo $row_ar_array[5]['rating_cat_describtion'];?>';
                    }
                    else{
                        return '<?php echo $row_ar_array[5]['rating_cat_describtion'];?>';
                    }
                    //return this.value;
                }

            },
             
        },

        yAxis: {
            min: 0
        },

       tooltip: {
       
         shared: true,
            formatter: function () {

             if(this.x==0){
                return '<b>' + "<?php echo $row_ar_array[0]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg1+'/10' + '<br/> Percentile Score: <?php echo round($perc1);?>';
             }
             if(this.x==60){
                return '<b>' + "<?php echo $row_ar_array[1]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg2+'/10' + '<br/> Percentile Score: <?php echo round($perc2);?>';
             }
             if(this.x==120){
                return '<b>' + "<?php echo $row_ar_array[2]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg3+'/10' + '<br/> Percentile Score: <?php echo round($perc3);?>';
             }
             if(this.x==180){
                return '<b>' + "<?php echo $row_ar_array[3]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg4+'/10' + '<br/> Percentile Score: <?php echo round($perc4);?>';
             }
             if(this.x==240){
                return '<b>' + "<?php echo $row_ar_array[4]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg5+'/10' + '<br/> Percentile Score: <?php echo round($perc5);?>';
             }
             if(this.x==300){
                return '<b>' + "<?php echo $row_ar_array[5]['rating_cat_describtion'];?>" + '</b><br/>' + 'Candidate Score: ' + tavg6+'/10' + '<br/> Percentile Score: <?php echo round($perc6);?>';
             }

            } 
        },

        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 60
            },
        },
        
        series: [{
            type: 'area',
            name: 'Candidate Score',
            color: 'rgba(247, 204, 0,0.5)',
            data: [tavg1,tavg2,tavg3,tavg4,tavg5,tavg6]
        }]
    });
    

});
    </script>
	<script>
$(function(){
	$(".slidershowonclick").show();
});

$(function(){
	$(".view-result").click(function(){
		$(this).hide();
		$(".hide-result").show();
		$(".slidershowonclick").show();
	});
	
	$(".hide-result").click(function(){
		$(this).hide();
		$(".view-result").show();
		$(".slidershowonclick").hide();
	});
});
</script>
	
	<script>
			$(function() {
				var $elem = $('#content');
				
				$('#nav_up').fadeIn('slow');
				$('#nav_down').fadeIn('slow');  
				
				$(window).bind('scrollstart', function(){
					$('#nav_up,#nav_down').stop().animate({'opacity':'0.2'});
				});
				$(window).bind('scrollstop', function(){
					$('#nav_up,#nav_down').stop().animate({'opacity':'1'});
				});
				
				$('#nav_down').click(
					function (e) {
						$('html, body').animate({scrollTop: $elem.height()}, 800);
					}
				);
				$('#nav_up').click(
					function (e) {
						$('html, body').animate({scrollTop: '0px'}, 800);
					}
				);
				
				$(".view-result").click(function(){
					$(".slidershowonclick").show();
				});
				
				
            });
        </script>
						<script src="js/scroll-startstop.events.jquery.js" type="text/javascript"></script>
						
</head>

