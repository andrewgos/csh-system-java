<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Technical Recruiting | Talent Acquisition | Technical
	Hiring | CrowdSourceHire</title>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/style_front.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/default.css">
<link rel="stylesheet" href="css/jquery.qtip.min.css" />

<style>
.modal-content {
	border: 0 none;
	box-shadow: 0 5px 15px rgba(0, 0, 0, 0);
}

.innerBanner {
	height: 0;
}
</style>

</head>

<body style="background-color: #EAEBEF">

	<!--------------------------------------------------------------------->
	<!--                             HEADER                              -->
	<!--------------------------------------------------------------------->
	<div class="header" style="background-color: #EAEBEF; border: 0;">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<div class="logo">
						<a href="http://www.crowdsourcehire.com"><img
							src="img/logo.png" alt="logo"></a>
					</div>
				</div>
				<div class="col-sm-10">
					<div class="headerRight"></div>
				</div>
			</div>
		</div>
	</div>




	<div class="innerBanner">
		<div class="container">
			<div class="row">
				<div class="col-sm-12"></div>
			</div>
		</div>
	</div>

	<!--------------------------------------------------------------------->
	<!--                             TABS                                -->
	<!--------------------------------------------------------------------->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">

				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<div class="signup-box">
								<div class="register-menu">
								
									<ul class="clearfix" role="tablist">
										<li role="presentation" class="active"><a href="#signup" aria-controls="home" role="tab" data-toggle="tab">Sign Up</a></li>
										<li class=""><a href="#login" aria-controls="home" id='poplogin_op' role="tab" data-toggle="tab">Log In</a></li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="signup">
								
											<input type="text" required name='lastname' id='lastname_3' required placeholder="Company Name" class="field">
											<input type="text" required name='firstname' id='firstname_3' required placeholder="Name of the contact" class="field">
											<input type=hidden name=userType id=userType value="13" />
											<input type="email" name='email' id='email_3' required placeholder="Email" class="field">
											<input type="text" name='username' id='username_3' required placeholder="Username" class="field">
											<input type="password" name='password' id='password_3' required placeholder="Password" class="field">
											<input type="submit" value="Sign Up" id='submites_3' class="submit-btn">
										</div>
										<div role="tabpanel" class="tab-pane " id="login">
								
											<input type="text" id='loginusername' placeholder="Username" class="field">
											<input type="password" id='loginpass' placeholder="Password" class="field">
											<input type="submit" id='loginsubmit' value="Log In" class="submit-btn">
											<a href="#forgot" aria-controls="home" role="tab" data-toggle="tab" class='forgot'>Forgot password?</a>
								
										</div>
										<div role="tabpanel" class="tab-pane" id="forgot">
											<div class='alert alert-success' style='display: none' id='success-change'>User details is send to your email id</div>
											<span id="after_forgot" style='display: none;'><a href="register.php?login=1" class="submit-btn">Login</a></span>
											<input type="email" id='email_id_for' placeholder="Enter Email Id" class="field">
											<input type="submit" id='forgotsubmit' value="Reset Password" class="submit-btn">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>







	<!--------------------------------------------------------------------->
	<!--                             FOOTER                              -->
	<!--------------------------------------------------------------------->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="footlogo">
						<img src="img/foot-logo.png" alt="footer logo">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="footNav">
						<ul class="clearfix">
							<li><a href="http://www.crowdsourcehire.com/"
								target="_blank">About Us</a></li>
							<li><a href="http://www.crowdsourcehire.com/#founder"
								target="_blank">Who we are</a></li>
							<li><a
								href="http://www.crowdsourcehire.com/#separator-video"
								target="_blank">How it works</a></li>
							<li><a href="https://crowdsourcehire.zendesk.com/"
								target="_blank">Faq</a></li>
							<li><a href="http://www.crowdsourcehire.com/#contact-form"
								target="_blank">Contact us</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="copy">Copyright 2014 Crowd Source Hire</div>
				</div>
			</div>
		</div>
	</div>





	<!--------------------------------------------------------------------->
	<!--                             SCRIPTS                             -->
	<!--------------------------------------------------------------------->



	<script language="JavaScript" type="text/javascript"
		src="js/validations.js"></script>
	<script language=javascript>var controls = new Array('txtName','txtSurname','txtLogin','txtPass','txtEmail','drpCountries');var modes = new Array('empty','empty','an','an','email','empty');var errmsgs = new Array('Name Cannot Be Empty','Surname cannot be empty','Please enter the User Name in a valid format ( Alphanumeric Characters Only )','Please enter the Password in a valid format ( Alphanumeric Characters Only )','Please enter Email Address in a valid format','Country Cannot Be Empty');var values = new Array('','','','','','');var settings = new Array('','','','','','');</script>
	<script language="javascript">

	function checkform(){   
		document.form1.btnRegister.disabled=true;         
		var user_name= document.getElementById('txtLogin').value;
		var email= document.getElementById('txtEmail').value;
	       // alert(user_name);
	        var status=validate();
	        if(status){
	             $.post("register.php", { login_to_check: user_name,email: email, ajax: "yes" },
	             function(data){
	                 if(data==0){
	                     document.forms["form1"].submit();
	                 }
	                 else{
	                    alert(data);
			    		window.location("http://?php echo SERVER_NAME ?>/register.php");
	                    document.form1.btnRegister.disabled=false;
	                 }
	            });
	        }
	        else{
	            document.form1.btnRegister.disabled=false;
	        } 
	}

	</script>

	<script type="text/javascript"
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
	<script type="text/javascript">
	function showonlyone(thechosenone) {
	     $('.newboxes').each(function(index) {
	          if ($(this).attr("id") == thechosenone) {
	               $(this).show(200);
	          }
	          else {
	               $(this).hide(600);
	          }
	     });
	}
	</script>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.actual.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
	$(document).ready(function(){
	     
		//* boxes animation
		form_wrapper = $('.login_box');
		function boxHeight() {
			form_wrapper.animate({ marginTop : ( - ( form_wrapper.height() / 2) - 24) },400);	
		};
		form_wrapper.css({ marginTop : ( - ( form_wrapper.height() / 2) - 24) });
		$('.linkform a,.link_reg a').on('click',function(e){
			var target	= $(this).attr('href'),
			target_height = $(target).actual('height');
			$(form_wrapper).css({
				'height'		: form_wrapper.height()
			});	
			$(form_wrapper.find('form:visible')).fadeOut(400,function(){
				form_wrapper.stop().animate({
				height	 : target_height,
				marginTop: ( - (target_height/2) - 24)
				},500,function(){
					$(target).fadeIn(400);
					$('.links_btm .linkform').toggle();
					$(form_wrapper).css({
						'height'		: ''
					});	
				});
			});
			e.preventDefault();
		});
			
	//* validation
	});
</script>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- bootstrap plugins -->
	<script src="js/bootstrap.plugins.min.js"></script>
	<!-- tooltips -->
	<script src="js/jquery.qtip.min.js"></script>
	<script src="js/jquery.ui.totop.min.js"></script>
	<script src="js/gebo_common.js"></script>
	<script src="js/jquery-ui-1.8.23.custom.min.js"></script>

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-42984503-1', 'auto');
	ga('send', 'pageview');
</script>

	<script>
	function validateForm(x) {	 
		var atpos = x.indexOf("@");
		var dotpos = x.lastIndexOf(".");
		if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			return false;
		}
		else{
			return true;
		}
	}
            
            
        $('#forgotsubmit').click(function(){
         var email_id=$('#email_id_for').val();
          //  alert(email_id);
        $.ajax({
              url: "ajaxrequest/forgotpassword.php",
              type: "POST",
              data: { email:email_id },
              dataType: "html",
              success:function(result){
                  if(result){
                      alert(result);
                  }
                  else{
                      $('.signup-box .register-menu ul li').removeClass('active');
                      $('#success-change').show();
                      $('#after_forgot').show();
                      $('#email_id_for').hide();
                      $('#forgotsubmit').hide();
                  }
              }
        });
    });
    
    

       $('#loginsubmit').click(function(){
       
 
        if($('#loginusername').val()==''){
              $('#loginusername').css('border','1px solid red');
        }
        else{
            $('#loginusername').css('border','0');
        }
        
       if($('#loginpass').val()=='')
        {
            $('#loginpass').css('border','1px solid red');
        }
        else{
            $('#loginpass').css('border','0');
        }
      
        if($('#loginpass').val()!='' && $('#loginusername').val()!=''){    
            $.ajax({
              url: "company",
              type: "POST",
              data: { password : $('#loginpass').val(), username:$('#loginusername').val() },
              dataType: "html",
              success:function(result){
                  if(result=='true'){
                      window.location.assign('http://app.crowdsourcehire.com/company/index.php');
                      window.print(result);
                  }
                else{ 
                	
                      window.alert(result);
                }
              }
            });
          }
     });
     
     $('#close').click(function(){

     		$("#success-change").hide();
     		$("#email_id_for").val("");

     	});
     
     $('#submites_3').click(function(){
     
        if($('#username_3').val()==''){
            
             
              $('#username_3').css('border','1px solid red');
        }
        else{
            $('#username_3').css('border','0');
        }
        
          if($('#email_3').val()=='' || !validateForm($('#email_3').val()))
        {
            //alert('test');
            $('#email_3').css('border','1px solid red');
        }
        else{
            $('#email_3').css('border','0');
        }
      
        if($('#password_3').val()=='')
        {
            $('#password_3').css('border','1px solid red');
        }
        else{
            $('#password_3').css('border','0');
        }
        
         if($('#firstname_3').val()=='')
        {
            $('#firstname_3').css('border','1px solid red');
        }
        else{
            $('#firstname_3').css('border','0');
        }
        
         if($('#lastname_3').val()=='')
        {
            $('#lastname_3').css('border','1px solid red');
        }
        else{
            $('#lastname_3').css('border','0');
        }
        
        
        
      
      
        
   
        if($('#username_3').val()!='' && $('#lastname_3').val()!='' && $('#firstname_3').val()!='' &&  $('#password_3').val()!='' &&  $('#email_3').val()!='' && validateForm($('#email_3').val())){
          var curls=window.location.href;
            $.ajax({
              url: "company",
              type: "POST",
              data: { doRegister : 1, userType : $('#userType').val(), password : $('#password_3').val(), firstname:$('#firstname_3').val(), cur_url : curls, lastname: $('#lastname_3').val(), city:'',  university:'', username:$('#username_3').val(), email:$('#email_3').val() },
              dataType: "html",
              success:function(result){
                  if(result!=''){
                      $('.fg2').show();
                      alert(result);
                  }
                else{   
                        /*$('#loginusername').val($('#username_3').val());
                        $('#loginpass').val($('#password_3').val());
                        $('#loginsubmit').click();*/  
                        window.location.assign('register.php?step=2');
                }
              }
            });
        }   
    });
</script>
















</body>
</html>