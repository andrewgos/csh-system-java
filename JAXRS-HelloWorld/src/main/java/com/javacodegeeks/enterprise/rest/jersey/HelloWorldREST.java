package com.javacodegeeks.enterprise.rest.jersey;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Path("/REST")
public class HelloWorldREST {
	String dblocation;
	String dbuser;
	String dbpass;
	
	String function = "";
	
	public HelloWorldREST() throws IOException
	{
			Properties prop = new Properties();
			InputStream in = getClass().getResourceAsStream("db.properties");
			prop.load(in);
			in.close();;
			dblocation = (prop.getProperty("dblocation"));
			dbuser = (prop.getProperty("dbuser"));
			dbpass = (prop.getProperty("dbpass"));
	}
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/getQuestions?id=50349
	@GET
	@Path("/getQuestions")
	public Response responseMsg( @PathParam("parameter") String parameter,
			@DefaultValue("Nothing to say") @QueryParam("id") String value) {
	
		function = "questions";
		String id = value; 
	    String sql  = "SELECT * FROM questions WHERE quiz_id="+id+"";
	    String output = runSQL(sql);	
		if (output.equals("[]"))
	    {
	    	String error = "no results found";
	    	return Response.status(200).entity(error).build();
	    }
		return Response.status(200).entity(output).build();
	}
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/getAssessments
	@GET
	@Path("/getAssessments")
	public Response responseMsg1( @PathParam("parameter") String parameter) {
	 
		function = "assessments";
	    String sql  = "SELECT quiz_name FROM quizzes";
	    String output = runSQL(sql);	
		if (output.equals("[]"))
	    {
	    	String error = "no results found";
	    	return Response.status(200).entity(error).build();
	    }
		return Response.status(200).entity(output).build();
	}
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/getProfile?id=50349
	@GET
	@Path("/getProfile")
	public Response responseMsg2( @PathParam("parameter") String parameter, @DefaultValue("Nothing to say") @QueryParam("id") String value) {
		
		String id = value;
		function = "profile";
	    String sql  = "SELECT * FROM user_profile where user_id ="+id+"";
	    String output = runSQL(sql);	
		if (output.equals("[]"))
	    {
	    	String error = "no results found";
	    	return Response.status(200).entity(error).build();
	    }
		return Response.status(200).entity(output).build();
	}
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/getJobs?id=50210
	@GET
	@Path("/getJobs")
	public Response responseMsg3( @PathParam("parameter") String parameter, @DefaultValue("Nothing to say") @QueryParam("id") String value) {
		
		String id = value;
		function = "jobs";
	    String sql  = "SELECT * FROM company_job_description where CompanyID ="+id+"";
	    String output = runSQL(sql);	
		if (output.equals("[]"))
	    {
	    	String error = "no results found";
	    	return Response.status(200).entity(error).build();
	    }
		return Response.status(200).entity(output).build();
	}
	
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/getCandidates?id=50210
	@GET
	@Path("/getCandidates")
	public Response responseMsg4( @PathParam("parameter") String parameter, @DefaultValue("Nothing to say") @QueryParam("id") String value) {
		
		String id = value;
		function = "candidates";
	    String sql  = "select assignment_users.user_id, users.Name, users.Surname, users.email, assignments.assignment_name from users, assignment_users, assignments where assignments.inserted_by="+id+" and assignments.id = assignment_users.assignment_id and users.UserID = assignment_users.user_id";
	    String output = runSQL(sql);	
		if (output.equals("[]"))
	    {
	    	String error = "no results found";
	    	return Response.status(200).entity(error).build();
	    }
		return Response.status(200).entity(output).build();
	}
	
	//USAGE: http://localhost:8080/JAXRS-HelloWorld/rest/REST/checkUser?email=dylan@crowdsourcehire.com
			@GET
			@Path("/checkUser")
			public Response responseMsg5( @PathParam("parameter") String parameter, @DefaultValue("Nothing to say") @QueryParam("email") String value) {
				
				String email = value;
				function = "user";
			    String sql  = "select * from users where email='"+email+"'";
			    String output = runSQL(sql);	
				if (output.equals("[]"))
			    {
			    	String error = "no results foundzz";
			    	// Insert User here
			    	return Response.status(200).entity(error).build();
			    }
				return Response.status(200).entity(output).build();
			}	
	
	
	public String runSQL(String sql)
	{
		String output = "";
		JSONArray list = new JSONArray();
	    try
	    {
	        Class.forName("com.mysql.jdbc.Driver");
	        Connection conn=null;
	        conn=DriverManager.getConnection(dblocation,dbuser,dbpass);
	        ResultSet rs=null;
	        Statement stm1=conn.createStatement();
	        rs=stm1.executeQuery(sql);
	        while(rs.next())
	        {
	            JSONObject obj=new JSONObject(); 
	            
	            switch (function) {
	            case "questions": 
	            	obj.put("id", rs.getString("id"));
	            	obj.put("question_text", rs.getString("question_text"));
	            	break;
	            case "assessments": 
	            	obj.put("quiz_name",rs.getString("quiz_name"));
	            	break;
	            case "profile": 
	            	obj.put("category",rs.getString("category"));
	            	obj.put("name",rs.getString("name"));
	            	obj.put("value",rs.getString("value"));
	            	break;
	            case "jobs": 
	            	obj.put("JD_ID",rs.getString("JD_ID"));
	            	obj.put("JdTitle",rs.getString("JdTitle"));
	            	obj.put("updated_date",rs.getString("updated_date"));
	            	//obj.put("AssessmentExpiration",rs.getString("AssessmentExpiration"));
	            	break;
	            case "candidates":
	            	obj.put("user_id",rs.getString("user_id"));
	            	obj.put("Name",rs.getString("Name"));
	            	obj.put("Surname",rs.getString("Surname"));
	            	obj.put("email",rs.getString("email"));
	            	obj.put("assignment_name",rs.getString("assignment_name"));
	            	break;
	            case "user":
	            	obj.put("UserID",rs.getString("UserID"));
	            	obj.put("UserName", rs.getString("UserName"));
	            	obj.put("Name",rs.getString("Name"));
	            	obj.put("Surname",rs.getString("Surname"));
	            	obj.put("email",rs.getString("email"));
	            	break;
				default: System.out.println("error");
	            }
	            
	            list.add(obj);
	        }	 
	    }
	    catch(Exception ex)
	    {
	    	output = ex.getMessage();
	    }
		output = list.toString();
		return output;
	}
}